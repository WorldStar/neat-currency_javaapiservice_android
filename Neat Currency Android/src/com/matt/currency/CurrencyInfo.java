package com.matt.currency;

public class CurrencyInfo {
	String cucode;
	String cuDetail;
	int cuFlags;
	String cuSymbol;
	public void setCuDetail(String details){
		this.cuDetail = details;
	}
	public String getCuDetail(){
		return cuDetail;
	}
	public void setCuCode(String code){
		this.cucode = code;
	}
	public String getCuCode(){
		return cucode;
	}
	public void setCuFlags(int flag){
		this.cuFlags = flag;
	}
	public int getCuFlags(){
		return cuFlags;
	}
	public void setCuSymbol(String symbol){
		this.cuSymbol = symbol;
	}
	public String getCuSymbol(){
		return cuSymbol;
	}
}
