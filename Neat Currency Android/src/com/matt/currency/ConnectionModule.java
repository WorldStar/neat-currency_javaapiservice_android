package com.matt.currency;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;



import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo.State;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.text.format.Time;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.jjoe64.graphview.GraphView.GraphViewData;

public class ConnectionModule {
   String currencyURL = "http://openexchangerates.org/api/currencies.json?API_KEY=CHANGE_ME_TO_YOUR_API_KEY";
   public String CurrencyDataReceive(){
	   Map<String, String> currencies = new HashMap<String, String>();
		   HttpClient httpclient = new DefaultHttpClient();
		   HttpParams params = httpclient.getParams();
		   HttpConnectionParams.setConnectionTimeout(params, 30000);
		   HttpConnectionParams.setSoTimeout(params, 30000);
		   HttpGet httpget = new HttpGet(currencyURL);
		   HttpResponse response;
	   try{
		   response = httpclient.execute(httpget);
		   HttpEntity entity = response.getEntity();
		   if (entity != null) {

	            // A Simple JSON Response Read
	            InputStream instream = entity.getContent();
	            // now you have the string representation of the HTML request
	            BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
	            StringBuilder sb = new StringBuilder();

	            String line = null;
	            try {
	                while ((line = reader.readLine()) != null) {
	                    sb.append(line + "\n");
	                }
	            } catch (IOException e) {
	                e.printStackTrace();
	                sb.append("");
	            }
	            instream.close();
	            return sb.toString();
	       }else{
	    	   return "";
	       }
		   //OERClient oerClient = getClient();
			
		   //currencies = oerClient.getCurrencies();
		   		   
		   
	   }catch(Exception e){
		   e.getStackTrace();
		   System.out.println(e.toString());
		   currencies.clear();
		   return "";
	   }
   }
	private OERClient getClient() {
		return new OERClient(
				null, 
				OERClient.DEFAULT_OER_URL, 
				OERClient.DEFAULT_CONNECT_TIMEOUT, 
				OERClient.DEFAULT_READ_TIMEOUT, 
				new PrintWriter(System.out), 
				RestClient.THROW_ALL_ERRORS);
	}
	public void GetRates() throws IOException, JSONException {
		OERClient oerClient = getClient();
		
		//Use known-to-be-good date.
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2012);
		cal.set(Calendar.MONTH, 4);
		cal.set(Calendar.DAY_OF_MONTH, 25);
		
		Map<String, Double> rates = oerClient.getRates(cal.getTime());
			
	}
	public void GetLatestRates() throws JSONException, IOException {
		OERClient oerClient = getClient();
		
		Map<String, Double> rates = oerClient.getLatestRates();
			
	}
	public void GetTimestamp() throws IOException, JSONException {
		OERClient oerClient = getClient();
		
		Date ts = oerClient.getTimestamp();
		
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(ts.getTime());
	}
   public boolean checkNextworkConnection(Context context){
	   try {
		   ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		   State wifi = conMan.getNetworkInfo(1).getState(); // wifi
		   if (wifi == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTING) {
		   return true;
		   }

		   State mobile = conMan.getNetworkInfo(0).getState(); // mobile ConnectivityManager.TYPE_MOBILE
		   if (mobile == NetworkInfo.State.CONNECTED || mobile == NetworkInfo.State.CONNECTING) {
		   return true;
		   }

	   } catch (NullPointerException e) {
		   return false;
	   }

	   return false;
   }
   boolean dataNotFound = false;
   public void GetRatesFromYahooService(Context con, String ratestring, ValueInfo valinfo){
	   
      dataNotFound = false;
      String baseDir = con.getApplicationContext().getFilesDir().getAbsolutePath();
      //download the rates from yahoo to a CSV file
      downloadFileViaHTTP (baseDir, ratestring);
      //read the file line
      String filePath = baseDir + "/" + "quotes.csv";
      //Log.d("download", "-> filePath = " + filePath);
      String[] crates = new String[154];
      try {
         // open the file for reading
         InputStream instream = new FileInputStream(filePath);
         // if file the available for reading
         if (instream != null) {
            // prepare the file for reading
            InputStreamReader inputreader = new InputStreamReader(instream);
            BufferedReader buffreader = new BufferedReader(inputreader);
            //read the line
            
            int i = 0;
            String line = null;
            while((line = buffreader.readLine()) != null){
            	crates[i] = line;
            	i++;
            }
            //Log.d("download", "fileLine = " + fileLine);
            instream.close();
         }
      } 
      catch (Exception ex) {
         // print stack trace.
    	  for(int i = 0; i < 154; i++){
    		  crates[i] = "0";
    	  }
      }
      valinfo.setCrates(crates);
   }
   public void downloadFileViaHTTP (String localPath, String ratestring) {
	   Log.d("download", "downloadFileViaHTTP...");

	   try {
	      //this is the Yahoo url
	      String urlFile = "http://download.finance.yahoo.com/d/quotes.csv?s=" + ratestring + "&f=l1&e=.csv";
	      Log.d("download","urlFile = " + urlFile);
	      URL url = new URL(urlFile);
	      //create the new connection 
	      HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
	      urlConnection.setRequestMethod("GET");
	      urlConnection.connect();

	      //pointer to the downloaded file path
	      String localFileName = localPath + "/" + "quotes.csv";
	      //this is the actual downloaded file
	      File MyFilePtrDest = new File(localFileName);
	      Log.d("download","localFileName = " + localFileName);

	      //this will be used in reading the data from the Internet
	      InputStream inputStream = urlConnection.getInputStream();

	      //this will be used to write the downloaded data into the file we created
	      FileOutputStream fileOutput = new FileOutputStream(MyFilePtrDest);

	      byte[] buffer = new byte[1024];
	      int bufferLength = 0; //used to store a temporary size of the buffer

	      //write buffer contents to file
	      while ((bufferLength = inputStream.read(buffer)) > 0 ) {
	         //add the data in the buffer to the file in the file output stream (the file on the sd card
	         fileOutput.write(buffer, 0, bufferLength);
	      }

	      inputStream.close();
	      //close the output stream when done
	      fileOutput.flush();
	      fileOutput.close();
	      urlConnection.disconnect();
	   }
	   catch (IOException e) {
	      //data were not found
	      dataNotFound = true;
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	   }
	} 
    public List<ChartInfo> getChartData(Context con, String basiccode, String othercode, int chartclickbt){
    	List<ChartInfo> temp = new ArrayList<ChartInfo>();
    	try {
			int[] todate = new int[3];
			int[] fromdate = new int[3];
			Calendar c = Calendar.getInstance();
    		switch(chartclickbt){
    		case 0:
    			/*Date currentTime = new Date();		
    			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    			String strdate = df.format(currentTime);
    			SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
    			sourceFormat.setTimeZone(TimeZone.getTimeZone("GMT-08:00"));
    	        Date parsed;
    			try {
    				parsed = sourceFormat.parse(strdate);
    		        TimeZone tz = TimeZone.getDefault(); 
    		        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
    		        destFormat.setTimeZone(tz);  

    		        strdate = destFormat.format(parsed);
    		        
    			} catch (ParseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} */
    			todate[0] = c.get(Calendar.DAY_OF_MONTH);
    			todate[1] = c.get(Calendar.MONTH) + 1;
    			todate[2] = c.get(Calendar.YEAR);
    			fromdate[0] = todate[0];
    			fromdate[1] = todate[1];
    			fromdate[2] = todate[2];
    			temp = getDataFromLocalServer(con, basiccode, othercode, chartclickbt, fromdate, todate, 1);
    			List<ChartInfo> temp1 = new ArrayList<ChartInfo>();
    			for(int i = temp.size()-1; i >= 0; i--){
    				temp1.add(temp.get(i));
    			}
    			temp.clear();
    			temp.addAll(temp1);
    			//data = getDataFromLocalServer(con, basiccode, othercode, chartclickbt);
    			break;
    		case 1:
    			todate[0] = c.get(Calendar.DAY_OF_MONTH);
    			todate[1] = c.get(Calendar.MONTH) + 1;
    			todate[2] = c.get(Calendar.YEAR);
    			fromdate[0] = todate[0] - 5;
    			fromdate[1] = todate[1];
    			fromdate[2] = todate[2];
    			if(fromdate[0] < 0){
    				fromdate[0] = 31 + fromdate[0];
    				fromdate[1] = fromdate[1] - 1;
    			}
    			temp = getDataFromLocalServer(con, basiccode, othercode, chartclickbt, fromdate, todate, 2);
    			//data = getDataFromLocalServer(con, basiccode, othercode, chartclickbt);
    			List<ChartInfo> temp2 = new ArrayList<ChartInfo>();
    			for(int i = temp.size()-1; i >= 0; i--){
    				temp2.add(temp.get(i));
    			}
    			temp.clear();
    			temp.addAll(temp2);
    			break;
    		case 2:
    			//3m
    			todate[0] = c.get(Calendar.DAY_OF_MONTH);
    			todate[1] = c.get(Calendar.MONTH);
    			todate[2] = c.get(Calendar.YEAR);
    			fromdate[0] = todate[0];
    			fromdate[1] = todate[1] - 3;
    			fromdate[2] = todate[2];
    			if(fromdate[1] < 0){
    				fromdate[1] = 12 + fromdate[1];
    				fromdate[2] = fromdate[2] - 1;
    			}
    			temp = getDataFromYahooServer(con, basiccode, othercode, chartclickbt, fromdate, todate, "d");
    			break;
    		case 3:
    			//6m
    			todate[0] = c.get(Calendar.DAY_OF_MONTH);
    			todate[1] = c.get(Calendar.MONTH);
    			todate[2] = c.get(Calendar.YEAR);
    			fromdate[0] = todate[0];
    			fromdate[1] = todate[1] - 6;
    			fromdate[2] = todate[2];
    			if(fromdate[1] < 0){
    				fromdate[1] = 12 + fromdate[1];
    				fromdate[2] = fromdate[2] - 1;
    			}
    			temp = getDataFromYahooServer(con, basiccode, othercode, chartclickbt, fromdate, todate, "d");
    			break;
    		case 4:
    			//1y
    			todate[0] = c.get(Calendar.DAY_OF_MONTH);
    			todate[1] = c.get(Calendar.MONTH);
    			todate[2] = c.get(Calendar.YEAR);
    			fromdate[0] = todate[0];
    			fromdate[1] = todate[1];
    			fromdate[2] = todate[2] - 1;
    			temp = getDataFromYahooServer(con, basiccode, othercode, chartclickbt, fromdate, todate, "d");
    			break;
    		case 5:
    			//2y
    			todate[0] = c.get(Calendar.DAY_OF_MONTH);
    			todate[1] = c.get(Calendar.MONTH);
    			todate[2] = c.get(Calendar.YEAR);
    			fromdate[0] = todate[0];
    			fromdate[1] = todate[1];
    			fromdate[2] = todate[2] - 2;
    			temp = getDataFromYahooServer(con, basiccode, othercode, chartclickbt, fromdate, todate, "w");
    			break;
    		case 6:
    			//5y
    			todate[0] = c.get(Calendar.DAY_OF_MONTH);
    			todate[1] = c.get(Calendar.MONTH);
    			todate[2] = c.get(Calendar.YEAR);
    			fromdate[0] = todate[0];
    			fromdate[1] = todate[1];
    			fromdate[2] = todate[2] - 5;
    			temp = getDataFromYahooServer(con, basiccode, othercode, chartclickbt, fromdate, todate, "w");
    			break;
    		}
  	      
  	   }
  	   catch (Exception e) {
  	      //data were not found
  		 temp = null;
  	      // TODO Auto-generated catch block
  	      e.printStackTrace();
  	   }
    	return temp;
    }


    public List<ChartInfo> getDataFromLocalServer(Context con, String basiccode, String othercode, int chartclickbt, int[] fromdate, int[] todate, int flg ){
    	
        List<ChartInfo> temp = new ArrayList<ChartInfo>();
    	try {
	        List<ChartInfo> crates = new ArrayList<ChartInfo>();
	        List<ChartInfo> crates1 = new ArrayList<ChartInfo>();
    		if(!basiccode.equals("USD")){
    			try{
		  	      //this is the Yahoo url
    				
		  	      //String urlFile = "http://115.68.56.41/web_data/ExRateValues/"+basiccode+"_quotes.csv";
    			  String urlFile = "http://www.myschola.jp/myvideo/include/ex/ExRateValues/"+basiccode+"_quotes.csv";
		  	     // Log.d("download","urlFile = " + urlFile);
		  	      URL url = new URL(urlFile);
		  	      //create the new connection 
		  	      HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		  	      urlConnection.setRequestMethod("GET");
		  	      urlConnection.connect();
		
		  	      //pointer to the downloaded file path
		  	      String baseDir = con.getApplicationContext().getFilesDir().getAbsolutePath();
		  	      String localFileName = baseDir + "/" + "chart_basic_quotes.csv";
		  	      //this is the actual downloaded file
		  	      File MyFilePtrDest = new File(localFileName);
		  	      //Log.d("download","localFileName = " + localFileName);
		
		  	      //this will be used in reading the data from the Internet
		  	      InputStream inputStream = urlConnection.getInputStream();
		
		  	      //this will be used to write the downloaded data into the file we created
		  	      FileOutputStream fileOutput = new FileOutputStream(MyFilePtrDest);
		
		  	      byte[] buffer = new byte[1024];
		  	      int bufferLength = 0; //used to store a temporary size of the buffer
		
		  	      //write buffer contents to file
		  	      while ((bufferLength = inputStream.read(buffer)) > 0 ) {
		  	         //add the data in the buffer to the file in the file output stream (the file on the sd card
		  	         fileOutput.write(buffer, 0, bufferLength);
		  	      }
		
		  	      inputStream.close();
		  	      //close the output stream when done
		  	      fileOutput.flush();
		  	      fileOutput.close();
		  	      urlConnection.disconnect();
		  	      
		  	      
		  	      //parsing
			  	    String filePath = baseDir + "/" + "chart_basic_quotes.csv";
			        try {
			           // open the file for reading
			           InputStream instream = new FileInputStream(filePath);
			           // if file the available for reading
			           if (instream != null) {
			              // prepare the file for reading
			              InputStreamReader inputreader = new InputStreamReader(instream);
			              BufferedReader buffreader = new BufferedReader(inputreader);
			              //read the line
			              
			              String line = null;
			              boolean ff = false;
			              String fromParse = fromdate[0] + "-" + fromdate[1]+"-"+fromdate[2] + " 12:01 AM"; 

			              long difftime = getDiffDate(fromParse);
			              long fromtime = getLocalGMTDate(fromParse);
			              fromtime = fromtime + difftime;
			              String toParse = todate[0] + "-" + todate[1]+"-"+ todate[2] + " 11:59 PM"; 
			              long totime = getLocalGMTDate(toParse);
			              totime = totime + difftime;
			              SimpleDateFormat realformatter = new SimpleDateFormat("MM/dd/yyyy HH:mm"); // I assume d-M, you may refer to M-d for month-day instead.
			              realformatter.setTimeZone(TimeZone.getDefault());
			              Calendar c4 = Calendar.getInstance();
			              while((line = buffreader.readLine()) != null){
			            	  /*if(!ff){
				            	  ff = true;
			            		  continue;
			            	  }*/
			            	  ChartInfo cinfo = new ChartInfo();
			            	  String[] a = line.split(",");
			            	  
			            	  String realparse = a[0]; 
			            	  realparse = realparse.replace("\"", "");
				              Date realdate = realformatter.parse(realparse); // You will need try/catch around this
				              long realtime = realdate.getTime();
				              Date realdate1 = getGMTDate(realparse);
				              SimpleDateFormat realformatter1 = new SimpleDateFormat("MM/dd/yyyy HH:mm"); // I assume d-M, you may refer to M-d for month-day instead.
				              String rdate = realformatter1.format(realdate1);
				              if(realtime > fromtime && realtime < totime){
				            	  cinfo.dt = rdate;
				            	  cinfo.crate = a[1];
				            	  crates.add(cinfo);
				              }
			              }
			              //Log.d("download", "fileLine = " + fileLine);
			              instream.close();
			           }
			        } 
			        catch (Exception ex) {
			           // print stack trace.
			        	crates.clear();
			        }
    			}catch(Exception e){
    				crates.clear();
    			}
    		}
    		if(!othercode.equals("USD")){
    			try{
		  	      //this is the Yahoo url
    			  //String urlFile = "http://115.68.56.41/web_data/ExRateValues/"+othercode+"_quotes.csv";
    				String urlFile = "http://www.myschola.jp/myvideo/include/ex/ExRateValues/"+othercode+"_quotes.csv";
		  	     // Log.d("download","urlFile = " + urlFile);
		  	      URL url = new URL(urlFile);
		  	      //create the new connection 
		  	      HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		  	      urlConnection.setRequestMethod("GET");
		  	      urlConnection.connect();
		
		  	      //pointer to the downloaded file path
		  	      String baseDir = con.getApplicationContext().getFilesDir().getAbsolutePath();
		  	      String localFileName = baseDir + "/" + "chart_other_quotes.csv";
		  	      //this is the actual downloaded file
		  	      File MyFilePtrDest = new File(localFileName);
		  	      //Log.d("download","localFileName = " + localFileName);
		
		  	      //this will be used in reading the data from the Internet
		  	      InputStream inputStream = urlConnection.getInputStream();
		
		  	      //this will be used to write the downloaded data into the file we created
		  	      FileOutputStream fileOutput = new FileOutputStream(MyFilePtrDest);
		
		  	      byte[] buffer = new byte[1024];
		  	      int bufferLength = 0; //used to store a temporary size of the buffer
		
		  	      //write buffer contents to file
		  	      while ((bufferLength = inputStream.read(buffer)) > 0 ) {
		  	         //add the data in the buffer to the file in the file output stream (the file on the sd card
		  	         fileOutput.write(buffer, 0, bufferLength);
		  	      }
		
		  	      inputStream.close();
		  	      //close the output stream when done
		  	      fileOutput.flush();
		  	      fileOutput.close();
		  	      urlConnection.disconnect();
		  	      
		  	      
		  	      //parsing
			  	    String filePath = baseDir + "/" + "chart_other_quotes.csv";
			        try {
			           // open the file for reading
			           InputStream instream = new FileInputStream(filePath);
			           // if file the available for reading
			           if (instream != null) {
			              // prepare the file for reading
			              InputStreamReader inputreader = new InputStreamReader(instream);
			              BufferedReader buffreader = new BufferedReader(inputreader);
			              //read the line
			              
			              String line = null;
			              boolean ff = false;
			              String fromParse = fromdate[0] + "-" + fromdate[1]+"-"+fromdate[2] + " 12:01 AM"; 

			              long difftime = getDiffDate(fromParse);
			              long fromtime = getLocalGMTDate(fromParse);
			              fromtime = fromtime + difftime;
			              String toParse = todate[0] + "-" + todate[1]+"-"+ todate[2] + " 11:59 PM"; 
			              long totime = getLocalGMTDate(toParse);
			              totime = totime + difftime;
			              SimpleDateFormat realformatter = new SimpleDateFormat("MM/dd/yyyy HH:mm"); // I assume d-M, you may refer to M-d for month-day instead.
			              realformatter.setTimeZone(TimeZone.getDefault());
			              Calendar c4 = Calendar.getInstance();
			              while((line = buffreader.readLine()) != null){
			            	  /*if(!ff){
				            	  ff = true;
			            		  continue;
			            	  }*/
			            	  ChartInfo cinfo = new ChartInfo();
			            	  String[] a = line.split(",");
			            	  
			            	  String realparse = a[0]; 
			            	  realparse = realparse.replace("\"", "");
				              Date realdate = realformatter.parse(realparse); // You will need try/catch around this
				              long realtime = realdate.getTime();
				              Date realdate1 = getGMTDate(realparse);
				              SimpleDateFormat realformatter1 = new SimpleDateFormat("MM/dd/yyyy HH:mm"); // I assume d-M, you may refer to M-d for month-day instead.
				              String rdate = realformatter1.format(realdate1);
				              if(realtime > fromtime && realtime < totime){
				            	  cinfo.dt = rdate;
				            	  cinfo.crate = a[1];
				            	  crates1.add(cinfo);
				              }
			              }
			              /*while((line = buffreader.readLine()) != null){
			            	  if(!ff){
				            	  ff = true;
			            		  continue;
			            	  }
			            	  ChartInfo cinfo = new ChartInfo();
			            	  String[] a = line.split(",");
			            	  cinfo.dt = a[0] + " " + a[1];
			            	  cinfo.crate = a[2];
			            	  crates1.add(cinfo);
			              }*/
			              //Log.d("download", "fileLine = " + fileLine);
			              instream.close();
			           }
			        } 
			        catch (Exception ex) {
			           // print stack trace.
			        	crates.clear();
			        }
    			}catch(Exception e){
    				crates.clear();
    			}
    		}
    		List<ChartInfo> crates2 = new ArrayList<ChartInfo>();
	        List<ChartInfo> crates3 = new ArrayList<ChartInfo>();
    		for(int i = crates.size() -1; i >=0; i-- ){
    			ChartInfo cinfo = crates.get(i);
    			crates2.add(cinfo);
    		}
    		for(int i = crates1.size() -1; i >=0; i-- ){
    			ChartInfo cinfo = crates1.get(i);
    			crates3.add(cinfo);
    		}
    		crates.clear();
    		crates.addAll(crates2);
    		crates1.clear();
    		crates1.addAll(crates3);
    		if(basiccode.equals("USD")){
    			temp = crates1;
    		}else if(othercode.equals("USD")){
    			for(int i = 0; i < crates.size(); i++){
    				ChartInfo cinfo = crates.get(i);
    				ChartInfo ct = new ChartInfo();
    				ct.dt = cinfo.dt;
    				try{
    					double ac = 1.0f / Double.parseDouble(cinfo.crate);
    					ct.crate = "" + ac;
    				}catch(Exception e){
    					ct.crate = "0";
    				}
    				temp.add(ct);
    			}
    			//temp = crates;
    		}else{
    			for(int i = 0; i < crates.size(); i++){
    				if( i > crates1.size() - 1) break;
    				ChartInfo cinfo = crates.get(i);
    				ChartInfo cinfo1 = crates1.get(i);
    				ChartInfo ct = new ChartInfo();
    				ct.dt = cinfo.dt;
    				try{
    					double ac = Double.parseDouble(cinfo1.crate) / Double.parseDouble(cinfo.crate);
    					ct.crate = "" + ac;
    				}catch(Exception e){
    					ct.crate = "0";
    				}
    				temp.add(ct);
    			}
    		}
    		
  	   }
  	   catch (Exception e) {
  	      //data were not found
  	      dataNotFound = true;
  	      temp.clear();
  	      // TODO Auto-generated catch block
  	      e.printStackTrace();
  	   }
    	List<ChartInfo> temp1 = new ArrayList<ChartInfo>();
		if(temp.size() > 200){
			int di = temp.size() / 200;
			if(di == 0)di = 1;
			for(int i = 0; i < temp.size(); i++){
				if(i % di == 0){
					temp1.add(temp.get(i));
				}
			}
		}else{
			temp1.addAll(temp);
		}
    	return temp1;
    }
    public List<ChartInfo> getDataFromYahooServer(Context con, String basiccode, String othercode, int chartclickbt, int[] fromdate, int[] todate, String g){
    	
        List<ChartInfo> temp = new ArrayList<ChartInfo>();
    	try {
	        List<ChartInfo> crates = new ArrayList<ChartInfo>();
	        List<ChartInfo> crates1 = new ArrayList<ChartInfo>();
    		if(!basiccode.equals("USD")){
    			try{
		  	      //this is the Yahoo url
		  	      String urlFile = "http://ichart.yahoo.com/table.csv?s="+basiccode+"=x&a="+fromdate[1]+"&b="+fromdate[0]+"&c="+fromdate[2]+"&d="+todate[1]+"&e="+todate[0]+"&f="+todate[2]+"&g="+g+"&ignore=.csv";
		  	     // Log.d("download","urlFile = " + urlFile);
		  	      URL url = new URL(urlFile);
		  	      //create the new connection 
		  	      HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		  	      urlConnection.setRequestMethod("GET");
		  	      urlConnection.connect();
		
		  	      //pointer to the downloaded file path
		  	      String baseDir = con.getApplicationContext().getFilesDir().getAbsolutePath();
		  	      String localFileName = baseDir + "/" + "chart_basic_quotes.csv";
		  	      //this is the actual downloaded file
		  	      File MyFilePtrDest = new File(localFileName);
		  	      //Log.d("download","localFileName = " + localFileName);
		
		  	      //this will be used in reading the data from the Internet
		  	      InputStream inputStream = urlConnection.getInputStream();
		
		  	      //this will be used to write the downloaded data into the file we created
		  	      FileOutputStream fileOutput = new FileOutputStream(MyFilePtrDest);
		
		  	      byte[] buffer = new byte[1024];
		  	      int bufferLength = 0; //used to store a temporary size of the buffer
		
		  	      //write buffer contents to file
		  	      while ((bufferLength = inputStream.read(buffer)) > 0 ) {
		  	         //add the data in the buffer to the file in the file output stream (the file on the sd card
		  	         fileOutput.write(buffer, 0, bufferLength);
		  	      }
		
		  	      inputStream.close();
		  	      //close the output stream when done
		  	      fileOutput.flush();
		  	      fileOutput.close();
		  	      urlConnection.disconnect();
		  	      
		  	      
		  	      //parsing
			  	    String filePath = baseDir + "/" + "chart_basic_quotes.csv";
			        try {
			           // open the file for reading
			           InputStream instream = new FileInputStream(filePath);
			           // if file the available for reading
			           if (instream != null) {
			              // prepare the file for reading
			              InputStreamReader inputreader = new InputStreamReader(instream);
			              BufferedReader buffreader = new BufferedReader(inputreader);
			              //read the line
			              
			              String line = null;
			              boolean ff = false;
			              while((line = buffreader.readLine()) != null){
			            	  if(!ff){
				            	  ff = true;
			            		  continue;
			            	  }
			            	  ChartInfo cinfo = new ChartInfo();
			            	  String[] a = line.split(",");
			            	  cinfo.dt = a[0];
			            	  cinfo.crate = a[1];
			            	  crates.add(cinfo);
			              }
			              //Log.d("download", "fileLine = " + fileLine);
			              instream.close();
			           }
			        } 
			        catch (Exception ex) {
			           // print stack trace.
			        	crates.clear();
			        }
    			}catch(Exception e){
    				crates.clear();
    			}
    		}
    		if(!othercode.equals("USD")){
    			try{
		  	      //this is the Yahoo url
		  	      String urlFile = "http://ichart.yahoo.com/table.csv?s="+othercode+"=x&a="+fromdate[1]+"&b="+fromdate[0]+"&c="+fromdate[2]+"&d="+todate[1]+"&e="+todate[0]+"&f="+todate[2]+"&g="+g+"&ignore=.csv";
		  	     // Log.d("download","urlFile = " + urlFile);
		  	      URL url = new URL(urlFile);
		  	      //create the new connection 
		  	      HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		  	      urlConnection.setRequestMethod("GET");
		  	      urlConnection.connect();
		
		  	      //pointer to the downloaded file path
		  	      String baseDir = con.getApplicationContext().getFilesDir().getAbsolutePath();
		  	      String localFileName = baseDir + "/" + "chart_other_quotes.csv";
		  	      //this is the actual downloaded file
		  	      File MyFilePtrDest = new File(localFileName);
		  	      //Log.d("download","localFileName = " + localFileName);
		
		  	      //this will be used in reading the data from the Internet
		  	      InputStream inputStream = urlConnection.getInputStream();
		
		  	      //this will be used to write the downloaded data into the file we created
		  	      FileOutputStream fileOutput = new FileOutputStream(MyFilePtrDest);
		
		  	      byte[] buffer = new byte[1024];
		  	      int bufferLength = 0; //used to store a temporary size of the buffer
		
		  	      //write buffer contents to file
		  	      while ((bufferLength = inputStream.read(buffer)) > 0 ) {
		  	         //add the data in the buffer to the file in the file output stream (the file on the sd card
		  	         fileOutput.write(buffer, 0, bufferLength);
		  	      }
		
		  	      inputStream.close();
		  	      //close the output stream when done
		  	      fileOutput.flush();
		  	      fileOutput.close();
		  	      urlConnection.disconnect();
		  	      
		  	      
		  	      //parsing
			  	    String filePath = baseDir + "/" + "chart_other_quotes.csv";
			        try {
			           // open the file for reading
			           InputStream instream = new FileInputStream(filePath);
			           // if file the available for reading
			           if (instream != null) {
			              // prepare the file for reading
			              InputStreamReader inputreader = new InputStreamReader(instream);
			              BufferedReader buffreader = new BufferedReader(inputreader);
			              //read the line
			              
			              String line = null;
			              boolean ff = false;
			              while((line = buffreader.readLine()) != null){
			            	  if(!ff){
				            	  ff = true;
			            		  continue;
			            	  }
			            	  ChartInfo cinfo = new ChartInfo();
			            	  String[] a = line.split(",");
			            	  cinfo.dt = a[0];
			            	  cinfo.crate = a[1];
			            	  crates1.add(cinfo);
			              }
			              //Log.d("download", "fileLine = " + fileLine);
			              instream.close();
			           }
			        } 
			        catch (Exception ex) {
			           // print stack trace.
			        	crates.clear();
			        }
    			}catch(Exception e){
    				crates.clear();
    			}
    		}
    		List<ChartInfo> crates2 = new ArrayList<ChartInfo>();
	        List<ChartInfo> crates3 = new ArrayList<ChartInfo>();
    		for(int i = crates.size() -1; i >=0; i-- ){
    			ChartInfo cinfo = crates.get(i);
    			crates2.add(cinfo);
    		}
    		for(int i = crates1.size() -1; i >=0; i-- ){
    			ChartInfo cinfo = crates1.get(i);
    			crates3.add(cinfo);
    		}
    		crates.clear();
    		crates.addAll(crates2);
    		crates1.clear();
    		crates1.addAll(crates3);
    		if(basiccode.equals("USD")){
    			temp = crates1;
    		}else if(othercode.equals("USD")){
    			for(int i = 0; i < crates.size(); i++){
    				ChartInfo cinfo = crates.get(i);
    				ChartInfo ct = new ChartInfo();
    				ct.dt = cinfo.dt;
    				try{
    					double ac = 1.0f / Double.parseDouble(cinfo.crate);
    					ct.crate = "" + ac;
    				}catch(Exception e){
    					ct.crate = "0";
    				}
    				temp.add(ct);
    			}
    			//temp = crates;
    		}else{
    			for(int i = 0; i < crates.size(); i++){
    				if( i > crates1.size() - 1) break;
    				ChartInfo cinfo = crates.get(i);
    				ChartInfo cinfo1 = crates1.get(i);
    				ChartInfo ct = new ChartInfo();
    				ct.dt = cinfo.dt;
    				try{
    					double ac = Double.parseDouble(cinfo1.crate) / Double.parseDouble(cinfo.crate);
    					ct.crate = "" + ac;
    				}catch(Exception e){
    					ct.crate = "0";
    				}
    				temp.add(ct);
    			}
    		}
    		
  	   }
  	   catch (Exception e) {
  	      //data were not found
  	      dataNotFound = true;
  	      temp.clear();
  	      // TODO Auto-generated catch block
  	      e.printStackTrace();
  	   }
    	return temp;
    }
    public GraphViewData[] getDataFromLocalServer(Context con, String basiccode, String othercode, int chartclickbt){
    	GraphViewData[] data = null;
    	try {
  	      //this is the Yahoo url
  	      String urlFile = "http://twitterstockresearch.com/exchange/ExRateValues/"+basiccode+"_quotes.csv";
  	     // Log.d("download","urlFile = " + urlFile);
  	      URL url = new URL(urlFile);
  	      //create the new connection 
  	      HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
  	      urlConnection.setRequestMethod("GET");
  	      urlConnection.connect();

  	      //pointer to the downloaded file path
  	      String baseDir = con.getApplicationContext().getFilesDir().getAbsolutePath();
  	      String localFileName = baseDir + "/" + "chart_temp_quotes.csv";
  	      //this is the actual downloaded file
  	      File MyFilePtrDest = new File(localFileName);
  	      //Log.d("download","localFileName = " + localFileName);

  	      //this will be used in reading the data from the Internet
  	      InputStream inputStream = urlConnection.getInputStream();

  	      //this will be used to write the downloaded data into the file we created
  	      FileOutputStream fileOutput = new FileOutputStream(MyFilePtrDest);

  	      byte[] buffer = new byte[1024];
  	      int bufferLength = 0; //used to store a temporary size of the buffer

  	      //write buffer contents to file
  	      while ((bufferLength = inputStream.read(buffer)) > 0 ) {
  	         //add the data in the buffer to the file in the file output stream (the file on the sd card
  	         fileOutput.write(buffer, 0, bufferLength);
  	      }

  	      inputStream.close();
  	      //close the output stream when done
  	      fileOutput.flush();
  	      fileOutput.close();
  	      urlConnection.disconnect();
  	      
  	      
  	      //parsing
	  	    String filePath = baseDir + "/" + "chart_temp_quotes.csv";
	        List<ChartInfo> crates = new ArrayList<ChartInfo>();
	        try {
	           // open the file for reading
	           InputStream instream = new FileInputStream(filePath);
	           // if file the available for reading
	           if (instream != null) {
	              // prepare the file for reading
	              InputStreamReader inputreader = new InputStreamReader(instream);
	              BufferedReader buffreader = new BufferedReader(inputreader);
	              //read the line
	              
	              int i = 0;
	              String line = null;
	              while((line = buffreader.readLine()) != null){
	            	  ChartInfo cinfo = new ChartInfo();
	            	  String[] a = line.split(" ");
	              	//crates[i] = line;
	              	i++;
	              }
	              //Log.d("download", "fileLine = " + fileLine);
	              instream.close();
	           }
	        } 
	        catch (Exception ex) {
	           // print stack trace.
	      	  
	        }
  	   }
  	   catch (IOException e) {
  	      //data were not found
  	      dataNotFound = true;
  	      // TODO Auto-generated catch block
  	      e.printStackTrace();
  	   }
    	return data;
    }
    public Bitmap getYahooChart(String dd, String ratestring){
    	Bitmap bm = null;
    	String chart_url = "http://chart.finance.yahoo.com/z?s="+ratestring+"=x&t="+dd+"&z=m";
    	
    	 try {
 	        URL url = new URL(chart_url);
 	        
 	        InputStream input = url.openStream();
 	        //input.reset();
 	        Bitmap myBitmap = BitmapFactory.decodeStream(input,null,null);
 	        
 	        return myBitmap;
 	    } 
 	    
 	    
 	    catch (IOException e) {
 	       
 	        return null;
 	    }		
    }
    public Date getGMTDate(String str){
    	SimpleDateFormat gFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm"); 
		gFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date parsed = null;
		try {
			parsed = gFormat.parse(str);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    	return parsed;
    }

    public long getDiffDate(String str){
    	Date currentTime = new Date();		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		df.setTimeZone(TimeZone.getDefault());
		str = df.format(currentTime);
		
    	Date tmp = null;
		Calendar c = Calendar.getInstance();
		c.setTime(currentTime);
		long cutime = c.getTimeInMillis();
		SimpleDateFormat gFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm"); 
		gFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date parsed = null;
		try {
			parsed = gFormat.parse(str);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		Calendar c1 = Calendar.getInstance();
		c1.setTime(parsed);
		long gtime = c1.getTimeInMillis();
		cutime = cutime - gtime;
    	return cutime;
    }

    public long getLocalGMTDate(String str){
    	Date tmp = null;
		SimpleDateFormat sourceFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm aa"); 
		sourceFormat.setTimeZone(TimeZone.getDefault());
		try {
			tmp = sourceFormat.parse(str);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(tmp);
		long cutime = c.getTimeInMillis();
    	return cutime;
    }
    public static String timeZone()
    {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"), Locale.getDefault());
        String   timeZone = new SimpleDateFormat("Z").format(calendar.getTime());
        return timeZone.substring(0, 3) + ":"+ timeZone.substring(3, 5);
    }
}
