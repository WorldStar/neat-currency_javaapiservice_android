package com.matt.currency;

import com.matt.currency.utils.MyButton;
import com.matt.currency.utils.MyTextView;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;

/** Class Must extends with Dialog */
/** Implement onClickListener to dismiss dialog when OK Button is pressed */
public class CustomizeDialog extends Dialog implements OnClickListener {
	MyButton okButton, noButton;
	MyTextView content;
	String message = "";
	int activityid = 0;
	int pos = 0;
	Context context;
	public CustomizeDialog(Context context, String message, int pos, int activityid) {
		super(context);
		/** 'Window.FEATURE_NO_TITLE' - Used to hide the title */
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		/** Design the dialog in main.xml file */
		setContentView(R.layout.dialog);
		this.context = context;
		this.pos = pos;
		this.message = message;
		this.activityid = activityid;
		okButton = (MyButton) findViewById(R.id.yesbt);
		noButton = (MyButton) findViewById(R.id.nobt);
		content = (MyTextView) findViewById(R.id.contentView);
		content.setText(message);
		if(activityid == 3){
			okButton.setVisibility(View.GONE);
			noButton.setText("Close");
		}
		okButton.setOnClickListener(this);
		noButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		/** When OK Button is clicked, dismiss the dialog */
		if (v.getId() == R.id.yesbt){
			switch(activityid){
			case 1:
				((FavoActivity)context).DeleteProcessing(pos);
				dismiss();
				break;
			case 2:
				((AddFavoActivity)context).SaveProcessing();
				dismiss();
			}
		}else if(v.getId() == R.id.nobt){
			dismiss();
		}
	}

}
