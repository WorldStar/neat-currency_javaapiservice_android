package com.matt.currency;

public class ValueInfo {
	protected boolean bottombtflg = false;
	protected int selectedleft = 1;
	protected int selectedright = 8;
	protected long mintime = 0;
	protected String strdate = "";
	protected boolean threadflg = false;
	protected String[] crates = new String[154];
	protected String leftnum = "100";
	public ValueInfo(){
		for(int i = 0; i < crates.length; i++){
			crates[i] = "0";
		}
	}
	public void setLeftNum(String num){
		this.leftnum = num;
	}
	public String getLeftNum(){
		return leftnum;
	}
	
	public void setCrates(String[] rates){
		this.crates = rates;
	}
	public String[] getCrates(){
		return crates;		
	}
	public String getCrate(int i){
		return crates[i];
	}
	public void setBottomBt(boolean flg){
		this.bottombtflg = flg;
	}
	public boolean getBottomBt(){
		return bottombtflg;
	}
	public void setSelectedLeft(int index){
		this.selectedleft = index;
	}
	public int getSelectedLeft(){
		return selectedleft;
	}
	public void setSelectedRight(int index){
		this.selectedright = index;
	}
	public int getSelectedRight(){
		return selectedright;
	}
	public void setTime(long time){
		this.mintime = time;
	}
	public long getTime(){
		return mintime;
	}
	public void setStrDate(String date){
		this.strdate = date;
	}
	public String getStrDate(){
		return strdate;
	}
	public void setThreadFlg(boolean flg){
		this.threadflg = flg;
	}
	public boolean getThreadFlg(){
		return threadflg;
	}
	
}
