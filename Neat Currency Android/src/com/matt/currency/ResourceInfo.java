package com.matt.currency;

public class ResourceInfo {
	int removeid;
	int infoid;
	public void setRemoveId(int removeid){
		this.removeid = removeid;
	}
	public int getRemoveId(){
		return removeid;
	}
	public void setInfoId(int infoid){
		this.infoid = infoid;
	}
	public int getInfoId(){
		return infoid;
	}
}
