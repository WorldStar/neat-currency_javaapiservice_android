package com.matt.currency;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;


public class FavoStore {
	Context context;
	SharedPreferences preferences;
	SharedPreferences.Editor editor;
	String FAVO_LIST = "favolist";
	public FavoStore(Context context){
		this.context = context;
		preferences = context.getSharedPreferences("iExchangeFavo",	Context.MODE_PRIVATE);
		editor = preferences.edit();
	}
	
	public int[] getFavourites() {
		String string = preferences.getString(FAVO_LIST, "");
		int[] list = getListFromString(string);
		if(list == null || list.length == 1){
			list = new int[13];
			String s = "";
			for(int i = 0; i < 13; i++){
				list[i] = i;
				if(i == 0)
					s = "" + i;
				else s = s + "," + i;
			}
			saveFavourites(s);
		}
		return list;
	}

	public void saveFavourites( String list) {
		editor.putString(FAVO_LIST, list);
		commit();
	}
	
	private int[] getListFromString (String string){
		int[] temp = null;
		try{
			String[] list = string.split(","); 
			temp = new int[list.length];
			for(int i = 0; i < list.length; i++){
				temp[i] = Integer.parseInt(list[i]);
			}
		}catch(Exception e){
			
		}
		
		return temp;
	}	

	public void commit() {
		editor.commit();
	}

	public void clear() {
		editor.clear();
		commit();
	}
}
