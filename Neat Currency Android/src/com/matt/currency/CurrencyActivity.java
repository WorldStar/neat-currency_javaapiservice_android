package com.matt.currency;


import static com.nineoldandroids.view.ViewPropertyAnimator.animate;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ViewFlipper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.matt.currency.utils.MyButton;
import com.matt.currency.utils.MyTextView;
import com.matt.currency.wheel.AbstractWheelTextAdapter;
import com.matt.currency.wheel.OnWheelScrollListener;
import com.matt.currency.wheel.OnWheelChangedListener;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.GraphViewStyle;
import com.jjoe64.graphview.LineGraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.matt.currency.wheel.WheelView;


public class CurrencyActivity extends Activity implements OnClickListener{
	LinearLayout topLayout, middleLayout;
	ViewFlipper bottomFlipper;
	LinearLayout calLayout, mLayout, graphLayout,  favoLayout, dateLayout, landtitleLayout, cubtLayout;
	RelativeLayout bottomLayout;
	Button calbt, mainbt, graphbt, favobt;
	MyButton favorites, currencies;
	ImageView  topleftflag, toprightflag;
	RelativeLayout topRlayout;
	MyTextView topleftcode, topleftcountry, toprightcode, toprightcountry, leftnum, rightnum, leftvalue, rightvalue, centervalue;
	int width = 0;
	int height = 0;
	int graphflg = 0;
	GraphView graphView;
    static final int CURRENCY_SHOW = 0;
    static final int CALCULATE_SHOW = 1;
    static final int GRAPH_SHOW = 2;
    static final int FAVORITE_SHOW = 3;
	WheelView basiclist, otherlist;
	List<CurrencyInfo> currencylist = new ArrayList<CurrencyInfo>();
	List<CurrencyInfo> templist = new ArrayList<CurrencyInfo>();
	List<CurrencyInfo> favolist = new ArrayList<CurrencyInfo>();
	List<CurrencyInfo> favostorelist = new ArrayList<CurrencyInfo>();
	FlagInfo countryflags = new FlagInfo();
	ConnectionModule con;
	JSONDataParser parser;
	ValueInfo valinfo ;
	String[] flags;
	String[] codes;
	String[] details;
	String[] symbols;
	private Toast toast;
	private String rightValue = "0.0";
	private long lastBackPressTime = 0;
	MyButton bt0, bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9, btdel, btclear, btpoint;
	ImageView refreshbt;
	int chartclickbt = 0;
	boolean slideflg = false;
	FavoriteAdapter fadapter = null;
	CurrenciesAdapter cadapter = null;
	boolean directflg = false;
	boolean startflg = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_currency);
    	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		Display display = getWindowManager().getDefaultDisplay();
		width = display.getWidth();
		height = display.getHeight();
		con = new ConnectionModule();
		valinfo = new ValueInfo();
		parser = new JSONDataParser();
		valinfo.setThreadFlg(true);
		
		
		
		//FavoStore fstore = new FavoStore(CurrencyActivity.this);
		
		//fstore.saveFavourites("");
		
		flags = getResources().getStringArray(R.array.currency_flags);
		codes = getResources().getStringArray(R.array.currency_codes);
		details = getResources().getStringArray(R.array.currency_details);
		symbols = getResources().getStringArray(R.array.corrency_symbols);
		topRlayout = (RelativeLayout)findViewById(R.id.TouchRelativeLayout);
		topLayout = (LinearLayout)findViewById(R.id.topLayout);
		topLayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, height * 3 / 10));
		middleLayout = (LinearLayout)findViewById(R.id.middleLayout);
		middleLayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, height * 1 / 10));
		bottomLayout = (RelativeLayout)findViewById(R.id.bottomLayout);
		bottomFlipper = (ViewFlipper)findViewById(R.id.bottomFlipper);
		cubtLayout = (LinearLayout)findViewById(R.id.cubtLayout);
		cubtLayout.setVisibility(View.GONE);
		int he = (int)((float)height * 5.6f / 10f);
		bottomLayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, he));
		refreshbt = (ImageView)findViewById(R.id.refreshbt);
		//LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, height * 6 / 10);
		//lp.setMargins(5,5,5,5);
		//graphLayout.setLayoutParams(lp);
		calbt = (Button)findViewById(R.id.calbt);
		mainbt = (Button)findViewById(R.id.mainbt);
		graphbt = (Button)findViewById(R.id.graphbt);
		favobt = (Button)findViewById(R.id.favobt);
		favorites = (MyButton)findViewById(R.id.favoritebt);
		currencies = (MyButton)findViewById(R.id.currencybt);
		favorites.setOnClickListener(this);
		currencies.setOnClickListener(this);
		
		mLayout = (LinearLayout)findViewById(R.id.mLayout);
		basiclist = (WheelView)findViewById(R.id.basicList1);
		//basiclist.setCacheColorHint(Color.TRANSPARENT);
		otherlist = (WheelView)findViewById(R.id.otherList1);
		//otherlist.setCacheColorHint(Color.TRANSPARENT);
		startflg = false;


		
		InitTop();
		new GetCurrenciesTask().execute();
		initUI(View.GONE);
		graphflg = 0;
	}
	public void InitTop(){
		topleftflag = (ImageView)findViewById(R.id.topleftflag);
		toprightflag = (ImageView)findViewById(R.id.toprightflag);
		topleftcode = (MyTextView)findViewById(R.id.topleftcode);
		topleftcountry = (MyTextView)findViewById(R.id.topleftcountry);
		toprightcode = (MyTextView)findViewById(R.id.toprightcode);
		toprightcountry = (MyTextView)findViewById(R.id.toprightcountry);
		leftnum = (MyTextView)findViewById(R.id.leftnum);
		rightnum = (MyTextView)findViewById(R.id.rightnum);
		leftvalue = (MyTextView)findViewById(R.id.leftvalue);
		rightvalue = (MyTextView)findViewById(R.id.rightvalue);
		centervalue = (MyTextView)findViewById(R.id.centervalue);

		topleftflag.setVisibility(View.GONE);
		toprightflag.setVisibility(View.GONE);
		topleftcode.setVisibility(View.GONE);
		topleftcountry.setVisibility(View.GONE);
		toprightcode.setVisibility(View.GONE);
		toprightcountry.setVisibility(View.GONE);
		leftnum.setVisibility(View.GONE);
		rightnum.setVisibility(View.GONE);
		leftvalue.setVisibility(View.GONE);
		rightvalue.setVisibility(View.GONE);
		//centervalue.setVisibility(View.GONE);
		centervalue.setText("Updating...");
		
	}
	public void rightScrollDownUp(int k){
		final int t1 = valinfo.getSelectedRight();
		final int kk = k;
		if(t1 < templist.size()){
			animate(toprightflag).setDuration(300);
			animate(toprightcode).setDuration(300);
			animate(toprightcountry).setDuration(300);
			animate(toprightflag).alpha(0);
			animate(toprightcode).alpha(0);
			animate(toprightcountry).alpha(0);
			new Thread() {
    		    @Override
    		    public void run() {
    		    	try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    		    	CurrencyActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							otherlist.setCurrentItem(t1 + kk);
							animate(toprightflag).alpha(1);
							animate(toprightcode).alpha(1);
							animate(toprightcountry).alpha(1);
							if(graphflg == 1){
								drawGraph();
							}
							//toprightcountry.setGravity(Gravity.RIGHT);
						}
					});
    		    }
            }.start();
		}
	}
	public void leftScrollDownUp(int k){
		final int t1 = valinfo.getSelectedLeft();
		final int kk = k;
		if(t1 < favolist.size()){
			animate(topleftflag).setDuration(300);
			animate(topleftcode).setDuration(300);
			animate(topleftcountry).setDuration(300);
			animate(topleftflag).alpha(0);
			animate(topleftcode).alpha(0);
			animate(topleftcountry).alpha(0);
			new Thread() {
    		    @Override
    		    public void run() {
    		    	try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    		    	CurrencyActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							basiclist.setCurrentItem(t1 + kk);
							animate(topleftflag).alpha(1);
							animate(topleftcode).alpha(1);
							animate(topleftcountry).alpha(1);
							if(graphflg == 1){
								drawGraph();
							}
							
						}
					});
    		    }
            }.start();
		}
	}
	public void changeLeftToRight(){
		//refresh rotate
		RotateAnimation anim = new RotateAnimation(360f, 180f, refreshbt.getWidth() / 2.0f, refreshbt.getHeight()/ 2.0f);
		anim.setInterpolator(new LinearInterpolator());
		anim.setRepeatCount(0);
		anim.setDuration(200);
		refreshbt.startAnimation(anim);
		
		// change left to right
		int topwidth = width;
		int flag_width = 57 + topleftflag.getWidth();
		int flag_left = 3;
		int xval = topwidth - flag_width;
		int c_width = 57 + topleftcountry.getWidth();
		int c1_width = 57 + toprightcountry.getWidth();
		int xval1 = topwidth - c_width;
		int xval2 = topwidth - c1_width;
		int y = topleftflag.getHeight() + 10;
		ImageView flagtemp = null;
		MyTextView codetemp = null;
		MyTextView ctemp = null;
		if(slideflg){
			slideflg = false;
		}
		else{
			slideflg = true;
		}

		animate(topleftflag).setDuration(200);
		animate(toprightflag).setDuration(200);
		animate(topleftcode).setDuration(100);
		animate(toprightcode).setDuration(100);
		animate(topleftcountry).setDuration(100);
		animate(toprightcountry).setDuration(100);
		animate(topleftflag).x(xval).y(0);
		animate(topleftcode).x(xval - topleftcode.getWidth() - 10).y(0);
		animate(topleftcountry).x(xval1).y(y);
        animate(toprightflag).x(flag_left).y(0);
        animate(toprightcode).x(flag_left + toprightflag.getWidth() + 10).y(0);
        animate(toprightcountry).x(flag_left).y(y);
		flagtemp = topleftflag;
		topleftflag = toprightflag;
		toprightflag = flagtemp;
		codetemp = topleftcode;
		topleftcode = toprightcode;
		toprightcode = codetemp;
		ctemp = topleftcountry;
		topleftcountry = toprightcountry;
		toprightcountry = ctemp;
		
		RelativeLayout.LayoutParams left_param = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT); 
		left_param.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		left_param.addRule(RelativeLayout.BELOW, topleftflag.getId());
		left_param.setMargins(1, 0, 0, 5);
		topleftcountry.setLayoutParams(left_param);
		RelativeLayout.LayoutParams right_param = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT); 
		right_param.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		right_param.addRule(RelativeLayout.BELOW, toprightflag.getId());
		right_param.setMargins(0, 0, 1, 5);
		toprightcountry.setLayoutParams(right_param);
		//topleftcountry.setGravity(Gravity.LEFT);
		//if(slideflg){
            
		/*}else{
			animate(topleftflag).x(flag_left).y(0);
			animate(topleftcode).x(flag_left + topleftflag.getWidth()+10).y(0);
			animate(topleftcountry).x(flag_left).y(y);
            animate(toprightflag).x(xval).y(0);
            animate(toprightcode).x(xval - toprightcode.getWidth() - 10).y(0);
            animate(toprightcountry).x(xval2).y(y);
		}*/
		scrollChangedProcessing();
		
	}
	public void scrollChangedProcessing(){
		List<CurrencyInfo> tlist = new ArrayList<CurrencyInfo>();
		int t1 = valinfo.getSelectedLeft();
		int t2 = valinfo.getSelectedRight();
		//valinfo.setSelectedLeft(valinfo.getSelectedRight());
		//valinfo.setSelectedRight(t1);

		tlist.addAll(favolist);
		favolist.clear();
		favolist.addAll(templist); 
		templist.clear();
		templist.addAll(favolist);
		otherlist.setCurrentItem(t1);
		basiclist.setCurrentItem(t2);
		if(graphflg == 1){
			drawGraph();
		}
			
			//changeTopData();
	}
	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		switch(view.getId()){
		case R.id.refreshbt:
			changeLeftToRight();
			break;
		case R.id.calbt:
	        if (bottomFlipper.getDisplayedChild() != CALCULATE_SHOW) {
	        	bottomFlipper.setInAnimation(CurrencyActivity.this, R.anim.anim_alpha_in);
	        	bottomFlipper.setOutAnimation(CurrencyActivity.this, R.anim.anim_alpha_out);
	        	bottomFlipper.setDisplayedChild(CALCULATE_SHOW);
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				graphflg = 0;
				calLayout = (LinearLayout)findViewById(R.id.calLayout);
				calbt.setBackgroundResource(R.drawable.menu_calculator_1);
				mainbt.setBackgroundResource(R.drawable.menu_currency_0);
				graphbt.setBackgroundResource(R.drawable.menu_chart_0);
				favobt.setBackgroundResource(R.drawable.menu_favorite_0);
				bt0 = (MyButton)findViewById(R.id.bt0);
				bt1 = (MyButton)findViewById(R.id.bt1);
				bt2 = (MyButton)findViewById(R.id.bt2);
				bt3 = (MyButton)findViewById(R.id.bt3);
				bt4 = (MyButton)findViewById(R.id.bt4);
				bt5 = (MyButton)findViewById(R.id.bt5);
				bt6 = (MyButton)findViewById(R.id.bt6);
				bt7 = (MyButton)findViewById(R.id.bt7);
				bt8 = (MyButton)findViewById(R.id.bt8);
				bt9 = (MyButton)findViewById(R.id.bt9);
				btdel = (MyButton)findViewById(R.id.btdel);
				btclear = (MyButton)findViewById(R.id.btclear);
				btpoint = (MyButton)findViewById(R.id.btpoint);
				bt0.setOnClickListener(btclick);
				bt1.setOnClickListener(btclick);
				bt2.setOnClickListener(btclick);
				bt3.setOnClickListener(btclick);
				bt4.setOnClickListener(btclick);
				bt5.setOnClickListener(btclick);
				bt6.setOnClickListener(btclick);
				bt7.setOnClickListener(btclick);
				bt8.setOnClickListener(btclick);
				bt9.setOnClickListener(btclick);
				btdel.setOnClickListener(btclick);
				btclear.setOnClickListener(btclick);
				btpoint.setOnClickListener(btclick);
	        }
			break;
		case R.id.mainbt:
	        if (bottomFlipper.getDisplayedChild() != CURRENCY_SHOW) {
	        	bottomFlipper.setInAnimation(CurrencyActivity.this, R.anim.anim_bottom_in);
	        	bottomFlipper.setOutAnimation(CurrencyActivity.this, R.anim.anim_top_out);
	        	bottomFlipper.setDisplayedChild(CURRENCY_SHOW);
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				graphflg = 0;
				calbt.setBackgroundResource(R.drawable.menu_calculator_0);
				mainbt.setBackgroundResource(R.drawable.menu_currency_1);
				graphbt.setBackgroundResource(R.drawable.menu_chart_0);
				favobt.setBackgroundResource(R.drawable.menu_favorite_0);
	        }
			break;
		case R.id.graphbt:
	        if (bottomFlipper.getDisplayedChild() != GRAPH_SHOW) {
	        	bottomFlipper.setInAnimation(CurrencyActivity.this, R.anim.anim_bottom_in);
	        	bottomFlipper.setOutAnimation(CurrencyActivity.this, R.anim.anim_top_out);
	        	bottomFlipper.setDisplayedChild(GRAPH_SHOW);
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
				dateLayout = (LinearLayout)findViewById(R.id.dateLayout);
				landtitleLayout = (LinearLayout)findViewById(R.id.landtitleLayout);
				graphLayout = (LinearLayout)findViewById(R.id.graphLayout);

				calbt.setBackgroundResource(R.drawable.menu_calculator_0);
				mainbt.setBackgroundResource(R.drawable.menu_currency_0);
				graphbt.setBackgroundResource(R.drawable.menu_chart_1);
				favobt.setBackgroundResource(R.drawable.menu_favorite_0);
				landtitleLayout.setVisibility(View.GONE);
				dateLayout.setVisibility(View.VISIBLE);
				graphflg = 1;
				//if(graphView == null){
					drawGraph();
				//}else{
					//graphView.redrawAll();
				//}
				//
				MyButton txbt1 = (MyButton)findViewById(R.id.txbt1);
				MyButton txbt2 = (MyButton)findViewById(R.id.txbt2);
				MyButton txbt3 = (MyButton)findViewById(R.id.txbt3);
				MyButton txbt4 = (MyButton)findViewById(R.id.txbt4);
				MyButton txbt5 = (MyButton)findViewById(R.id.txbt5);
				MyButton txbt6 = (MyButton)findViewById(R.id.txbt6);
				MyButton txbt7 = (MyButton)findViewById(R.id.txbt7);
				txbt1.setOnClickListener(chartclick);
				txbt2.setOnClickListener(chartclick);
				txbt3.setOnClickListener(chartclick);
				txbt4.setOnClickListener(chartclick);
				txbt5.setOnClickListener(chartclick);
				txbt6.setOnClickListener(chartclick);
				txbt7.setOnClickListener(chartclick);
	        }
			break;
		case R.id.favobt:
	        /*if (bottomFlipper.getDisplayedChild() != FAVORITE_SHOW) {
	        	bottomFlipper.setInAnimation(CurrencyActivity.this, R.anim.anim_bottom_in);
	        	bottomFlipper.setOutAnimation(CurrencyActivity.this, R.anim.anim_top_out);
	        	bottomFlipper.setDisplayedChild(FAVORITE_SHOW);
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				favoLayout = (LinearLayout)findViewById(R.id.favoLayout);

				calbt.setBackgroundResource(R.drawable.menu_calculator_0);
				mainbt.setBackgroundResource(R.drawable.menu_currency_0);
				graphbt.setBackgroundResource(R.drawable.menu_chart_0);
				favobt.setBackgroundResource(R.drawable.menu_favorite_1);
				
	        }*/
			Intent intent = new Intent(CurrencyActivity.this, FavoActivity.class);
			CurrencyActivity.this.startActivity(intent);
			overridePendingTransition(R.anim.anim_bottom_in, R.anim.anim_alpha_out);	
			graphflg = 0;
			break;	
		case R.id.favoritebt:
			favorites.setBackgroundResource(R.drawable.main_currency_screen_favorites_button_1);
			currencies.setBackgroundResource(R.drawable.main_currency_screen_allcurrencies_button_0);
			templist.clear();
			valinfo.setBottomBt(false);
			templist.addAll(favostorelist);

			int right = 0;
			for(int i = 0; i < favostorelist.size(); i++){
				if(favostorelist.get(i).getCuCode().equals("MYR")){
					right= i;
				}
			}
			valinfo.setSelectedRight(right);
			otherlist.setViewAdapter(new CurrenciesAdapter(CurrencyActivity.this));
			otherlist.setVisibleItems(2);
			otherlist.setCurrentItem(valinfo.getSelectedRight());
			otherlist.addChangingListener(changedListener);
			otherlist.addScrollingListener(scrolledListener);
			//changeTopData();
			break;
		case R.id.currencybt:
			favorites.setBackgroundResource(R.drawable.main_currency_screen_favorites_button_0);
			currencies.setBackgroundResource(R.drawable.main_currency_screen_allcurrencies_button_1);
			templist.clear();
			valinfo.setBottomBt(true);
			templist.addAll(currencylist);
			right = 0;
			for(int i = 0; i < currencylist.size(); i++){
				if(currencylist.get(i).getCuCode().equals("MYR")){
					right= i;
				}
			}
			valinfo.setSelectedRight(right);
			otherlist.setViewAdapter(new CurrenciesAdapter(CurrencyActivity.this));
			otherlist.setVisibleItems(2);
			otherlist.setCurrentItem(valinfo.getSelectedRight());
			otherlist.addChangingListener(changedListener);
			otherlist.addScrollingListener(scrolledListener);
			//changeTopData();
			break;
		}
	}
	
	private void drawGraph(){
		/*
		 * use Date as x axis label
		 */
		
		new GetChartImage().execute();
		
	}
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);
	    if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) { 
	    	//this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    	directflg = false;
			topLayout.setVisibility(View.VISIBLE);
			middleLayout.setVisibility(View.VISIBLE);
			landtitleLayout.setVisibility(View.GONE);
			dateLayout.setVisibility(View.VISIBLE);
			bottomLayout.setBackgroundResource(R.drawable.main_currency_screen_lists_bg);
			((LinearLayout)findViewById(R.id.blineLayout)).setVisibility(View.VISIBLE);
			graphView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int)((float)height * 5.0f / 10.0f)));
	    }
	    else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
	    	//this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	    	if(graphflg == 1){
	    		directflg =true;
				topLayout.setVisibility(View.GONE);
				middleLayout.setVisibility(View.GONE);
				landtitleLayout.setVisibility(View.VISIBLE);
				((MyTextView)findViewById(R.id.ltxtitle1)).setText(favolist.get(valinfo.getSelectedLeft()).getCuCode()+" to " + templist.get(valinfo.getSelectedRight()).getCuCode());
				((MyTextView)findViewById(R.id.ltxtitle2)).setText("1 "+favolist.get(valinfo.getSelectedLeft()).getCuSymbol()+" = " + templist.get(valinfo.getSelectedRight()).getCuSymbol());
				((MyTextView)findViewById(R.id.ltxtitle3)).setText(rightValue);
				
				dateLayout.setVisibility(View.VISIBLE);
				bottomLayout.setBackgroundColor(getResources().getColor(R.color.black));
				((LinearLayout)findViewById(R.id.blineLayout)).setVisibility(View.GONE);
				Display display = getWindowManager().getDefaultDisplay();
				
				graphView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, display.getHeight() - 130));
	    	}
	    }
	}
	private final View.OnClickListener chartclick = new View.OnClickListener() {
		public void onClick(View v) {
			((MyButton)findViewById(R.id.txbt1)).setBackgroundColor(getResources().getColor(R.color.black));
			((MyButton)findViewById(R.id.txbt2)).setBackgroundColor(getResources().getColor(R.color.black));
			((MyButton)findViewById(R.id.txbt3)).setBackgroundColor(getResources().getColor(R.color.black));
			((MyButton)findViewById(R.id.txbt4)).setBackgroundColor(getResources().getColor(R.color.black));
			((MyButton)findViewById(R.id.txbt5)).setBackgroundColor(getResources().getColor(R.color.black));
			((MyButton)findViewById(R.id.txbt6)).setBackgroundColor(getResources().getColor(R.color.black));
			((MyButton)findViewById(R.id.txbt7)).setBackgroundColor(getResources().getColor(R.color.black));
			switch(v.getId()){
			case R.id.txbt1:
				((MyButton)findViewById(R.id.txbt1)).setBackgroundResource(R.drawable.new_gloss_button);
				
				chartclickbt = 0;
				break;
			case R.id.txbt2:
				((MyButton)findViewById(R.id.txbt2)).setBackgroundResource(R.drawable.new_gloss_button);
				chartclickbt = 1;
				break;
			case R.id.txbt3:
				((MyButton)findViewById(R.id.txbt3)).setBackgroundResource(R.drawable.new_gloss_button);
				chartclickbt = 2;			
				break;
			case R.id.txbt4:
				((MyButton)findViewById(R.id.txbt4)).setBackgroundResource(R.drawable.new_gloss_button);
				chartclickbt = 3;			
				break;
			case R.id.txbt5:
				((MyButton)findViewById(R.id.txbt5)).setBackgroundResource(R.drawable.new_gloss_button);
				chartclickbt = 4;			
				break;
			case R.id.txbt6:
				((MyButton)findViewById(R.id.txbt6)).setBackgroundResource(R.drawable.new_gloss_button);
				chartclickbt = 5;			
				break;
			case R.id.txbt7:
				((MyButton)findViewById(R.id.txbt7)).setBackgroundResource(R.drawable.new_gloss_button);
				chartclickbt = 6;			
				break;
			}
			drawGraph();
		}
	};
	
	private final View.OnClickListener btclick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(leftnum.getText().toString().equals("0")){
				leftnum.setText("");
			}
			String a = leftnum.getText().toString();
			String[] array;
			if(a.contains(".")){
			   array = a.split(".");
			   if(array.length == 0) array = new String[]{a};
			}else
				array = new String[]{a};
			
			switch(v.getId()){
			case R.id.btdel:
				if(leftnum.length() == 1){
					leftnum.setText("0");
				}else if(leftnum.length() > 1){
					leftnum.setText(leftnum.getText().toString().substring(0,leftnum.length()-1));
				}
				break;
			case R.id.btclear:
				leftnum.setText("0");
				break;
			}
			
			if(a.length() < 22)
			if(array.length == 1 || (array.length > 1 && array[1].length() < 2)){
				switch(v.getId()){
				case R.id.bt0:
					leftnum.setText(leftnum.getText().toString() + "0");				
					break;
				case R.id.bt1:
					leftnum.setText(leftnum.getText().toString() + "1");
					break;
				case R.id.bt2:
					leftnum.setText(leftnum.getText().toString() + "2");
					break;
				case R.id.bt3:
					leftnum.setText(leftnum.getText().toString() + "3");
					break;
				case R.id.bt4:
					leftnum.setText(leftnum.getText().toString() + "4");
					break;
				case R.id.bt5:
					leftnum.setText(leftnum.getText().toString() + "5");
					break;
				case R.id.bt6:
					leftnum.setText(leftnum.getText().toString() + "6");
					break;
				case R.id.bt7:
					leftnum.setText(leftnum.getText().toString() + "7");
					break;
				case R.id.bt8:
					leftnum.setText(leftnum.getText().toString() + "8");
					break;
				case R.id.bt9:
					leftnum.setText(leftnum.getText().toString() + "9");
					break;
				case R.id.btpoint:
					if(!leftnum.getText().toString().contains("."))
						leftnum.setText(leftnum.getText().toString() + ".");
					break;
				}
			}
			valinfo.setLeftNum(leftnum.getText().toString());
			calculateFunc();
		}
	};
	
	private WheelView getWheel(int id)
	{
		return (WheelView) findViewById(id);
	}
	
    private class FavoriteAdapter extends AbstractWheelTextAdapter{
        Context context;
        // Index of current item
        int currentItem;
        // Index of item to be highlighted
        int currentValue;
        public FavoriteAdapter(Context context) {
            super(context, R.layout.favoritelist, NO_RESOURCE);
            //setItemTextResource1(R.id.country);
            setItemTextResource(R.id.cuCode);
        }
        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            View view = super.getItem(index, cachedView, parent);
            //ImageView img = (ImageView) view.findViewById(R.id.flag);
        	ImageView flag = (ImageView)view.findViewById(R.id.imgFlag);
        	flag.setImageResource(countryflags.getFlags(favolist.get(index).getCuFlags()));
        	MyTextView country = (MyTextView)view.findViewById(R.id.country);
        	//country.setTextColor(context.getResources().getColor(R.color.unselected_color));
        	//((TextView)view.findViewById(R.id.cuCode)).setTextColor(context.getResources().getColor(R.color.unselected_color));
        	String a = favolist.get(index).getCuDetail();
        	if(a.length() > 17){
        		country.setTextSize(11);
        	}else{
        		country.setTextSize(13);
        	}
        	country.setText(favolist.get(index).getCuDetail());
        	
        	/*InputStream ims;
        	String s = "smallflags/"+ favolist.get(index).getCuFlags() + ".png";
			try {
				ims = context.getAssets().open("smallflags/"+ favolist.get(index).getCuFlags() + ".png");
                // load image as Drawable
                Drawable d = Drawable.createFromStream(ims, null);
                // set image to ImageView
                flag.setImageDrawable(d);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
        	view.setTag(index);
            return view;
        }
        
        @Override
        public int getItemsCount() {
            return favolist.size();
        }
        
        @Override
        protected CharSequence getItemText(int index) {
            return favolist.get(index).getCuCode();
        }
		@Override
		protected CharSequence getItemText1(int index) {
			// TODO Auto-generated method stub
			return favolist.get(index).getCuDetail();
		}
    }

    private class CurrenciesAdapter extends AbstractWheelTextAdapter {
    	Context context;

        public CurrenciesAdapter(Context context) {
            super(context, R.layout.culist, NO_RESOURCE);
            //setItemTextResource1(R.id.country);
            setItemTextResource(R.id.cuCode);
        }
        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            View view = super.getItem(index, cachedView, parent);
            //ImageView img = (ImageView) view.findViewById(R.id.flag);
        	ImageView flag = (ImageView)view.findViewById(R.id.imgFlag);
        	flag.setImageResource(countryflags.getFlags(templist.get(index).getCuFlags()));
        	MyTextView country = (MyTextView)view.findViewById(R.id.country);
        	String a = templist.get(index).getCuDetail();
        	if(a.length() > 17){
        		country.setTextSize(11);
        	}else{
        		country.setTextSize(13);
        	}
        	country.setText(templist.get(index).getCuDetail());
        	//country.setTextColor(context.getResources().getColor(R.color.unselected_color));
        	//((TextView)view.findViewById(R.id.cuCode)).setTextColor(context.getResources().getColor(R.color.unselected_color));
        	/*InputStream ims;
			try {
				ims = context.getAssets().open("smallflags/"+ currencylist.get(index).getCuFlags() + ".png");
                // load image as Drawable
                Drawable d = Drawable.createFromStream(ims, null);
                // set image to ImageView
                flag.setImageDrawable(d);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
        	view.setTag(index);
            return view;
        }
        
        @Override
        public int getItemsCount() {
            return templist.size();
        }
        
        @Override
        protected CharSequence getItemText(int index) {
            return templist.get(index).getCuCode();
        }
		@Override
		protected CharSequence getItemText1(int index) {
			// TODO Auto-generated method stub
			return templist.get(index).getCuDetail();
		}
    }
    private class GetCurrenciesTask extends
    	AsyncTask<Void, Void, List<CurrencyInfo>> {
    		ProgressDialog MyDialog;
		@Override
		protected List<CurrencyInfo> doInBackground(Void... params) {
			String currencies = "";
			/*if(con.checkNextworkConnection(CurrencyActivity.this)){
				currencies = con.CurrencyDataReceive();	
			}else{
				currencylist.clear();
				favolist.clear();
			}*/
			/*List<Currency> javaCurrencies = new ArrayList<Currency>();
	        Locale[] locs = Locale.getAvailableLocales();

	        for(Locale loc : locs) {
	            try {
	            	javaCurrencies.add( Currency.getInstance( loc ) );
	            } catch(Exception exc)
	            {
	                // Locale not found
	            }
	        }*/
			
			List<CurrencyInfo> culist1 = parser.currencyParserFromStringData(codes,details,symbols, countryflags);
			
			currencylist.addAll(culist1);
			FavoStore fstore = new FavoStore(CurrencyActivity.this);
			int[] flist = fstore.getFavourites();
			for(int i = 0; i < currencylist.size(); i++){
				for(int j = 0; j < flist.length; j++){
					if(i == flist[j]){
						favolist.add(currencylist.get(i));
						break;
					}
				}
	        	/*if(currencylist.get(i).getCuCode().equals("USD")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("EUR")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("GBP")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("CUF")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("JPY")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("AUD")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("CAD")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("BRL")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("ILS")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("CNY")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("RUB")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("FJD")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("MYR")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("ALL")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("SZL")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("BIF")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("ZMK")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("HKD")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("TWD")){ 
	        		favolist.add(currencylist.get(i));
	        	}else if(currencylist.get(i).getCuCode().equals("ARS")){ 
	        		favolist.add(currencylist.get(i));
	        	}*/
	        }
			favostorelist.addAll(favolist);
			if(con.checkNextworkConnection(CurrencyActivity.this)){
				String ratestring = "";
				for(int i = 0; i < codes.length; i++){
					if(i == 0){
						ratestring = "USD" + codes[i] + "=X,";
					}else if(i == codes.length - 1){
						ratestring = ratestring + "USD" + codes[i] + "=X";
					}else{
						ratestring = ratestring + "USD" + codes[i] + "=X,";
					}
				}
				con.GetRatesFromYahooService(CurrencyActivity.this, ratestring, valinfo);
			}else{
				//centervalue.setText("Not Connection");
			}
            return currencylist;
        }

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//MyDialog = ProgressDialog.show(CurrencyActivity.this, "",
					//"Loading Currency Data... ", true);

			//MyDialog.setCancelable(false);
			
		}
		@Override
		protected void onPostExecute(List<CurrencyInfo> _result) {
			super.onPostExecute(_result);
			//MyDialog.dismiss();
			if(_result == null && _result.size() == 0){
				//dialog procee  --- "Network Connection failed. Please check network and try again."
				currencylist.clear();
			}else{
				//List<CurrencyInfo> currencylist1 = new ArrayList<CurrencyInfo>();
				//currencylist1.addAll(currencylist);
				//List<CurrencyInfo> favolist1 = new ArrayList<CurrencyInfo>();
				//favolist1.addAll(favolist);
				if(con.checkNextworkConnection(CurrencyActivity.this)){
					
				}else{
					centervalue.setText("Not Connection");
				}
				initUI(View.VISIBLE);
				
				templist.addAll(favolist);
				int left = 0;
				for(int i = 0; i < favolist.size(); i++){
					if(favolist.get(i).getCuCode().equals("USD")){
						left= i;
					}
				}
				valinfo.setSelectedLeft(left);
				int right = 0;
				for(int i = 0; i < templist.size(); i++){
					if(templist.get(i).getCuCode().equals("MYR")){
						right= i;
					}
				}
				valinfo.setSelectedRight(right);
				changeTopData();
				fadapter = new FavoriteAdapter(CurrencyActivity.this);
				basiclist.setViewAdapter(fadapter);
				basiclist.addChangingListener(changedListener);
				basiclist.addScrollingListener(scrolledListener);
				
				//basiclist.setAdapter(favoadapter);
				/*
				 wheel.setViewAdapter(new CountryAdapter(this));
				wheel.setVisibleItems(2);
				wheel.setCurrentItem(0);
				wheel.addChangingListener(changedListener);
				wheel.addScrollingListener(scrolledListener);
				 */
				//basiclist.setSmoothScrollbarEnabled(true);
				cadapter = new CurrenciesAdapter(CurrencyActivity.this);
				otherlist.setViewAdapter(cadapter);
				otherlist.addChangingListener(changedListener);
				otherlist.addScrollingListener(scrolledListener);
				//otherlist.setAdapter(cuadapter);
				//otherlist.setSmoothScrollbarEnabled(true);
				cubtLayout.setVisibility(View.VISIBLE);
				//basiclist.setCyclic(true);
				//basiclist.setVisibleItems(2);
				basiclist.setCurrentItem(valinfo.getSelectedLeft());
				//otherlist.setCyclic(true);
				//otherlist.setVisibleItems(2);
				otherlist.setCurrentItem(valinfo.getSelectedRight());
				new GetThread().start();
				
			}
	    	  
	    }
    }
    

    private class GetChartImage extends
    	AsyncTask<Void, Void, List<ChartInfo>> {
    		ProgressDialog MyDialog;
		@Override
		protected List<ChartInfo> doInBackground(Void... params) {
			//Bitmap chartimage = null;
			//chartimage = con.getYahooChart("1d", "USDEUR");
			
			List<ChartInfo> temp = new ArrayList<ChartInfo>();
			if(con.checkNextworkConnection(CurrencyActivity.this)){
				temp = con.getChartData(CurrencyActivity.this, favolist.get(valinfo.getSelectedLeft()).getCuCode(), templist.get(valinfo.getSelectedRight()).getCuCode(), chartclickbt);
			}
			/**
			 * graph process
			 */
			if(temp != null && temp.size() != 0){
				GraphViewData[] data = new GraphViewData[temp.size()];
				String[] verlabels = new String[6];
				String[] verlabels1 = new String[6];
				String[] horlabels = new String[5];
				switch(chartclickbt){
				case 0:
					/*Calendar c = Calendar.getInstance();
					int[] todate = new int[3];
					todate[0] = c.get(Calendar.DAY_OF_MONTH);
	    			todate[1] = c.get(Calendar.MONTH) + 1;
	    			todate[2] = c.get(Calendar.YEAR);
	    			String fromParse = todate[1] + "/" + todate[0]+"/"+todate[2] + " 00:00";
	    			SimpleDateFormat sourceFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm"); 
	    			Date fromdate1 = null;
	    			long fromtime = 0;
					try {
						fromdate1 = sourceFormat.parse(fromParse);
						fromtime = fromdate1.getTime();
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					String toParse = todate[1] + "/" + todate[0]+"/"+todate[2] + " 23:59";
	    			Date todate1 = null;
	    			long totime = 0;
					try {
						todate1 = sourceFormat.parse(toParse);
						totime = todate1.getTime();
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					// processing chart parts with not data
					List<ChartInfo> temp1 = new ArrayList<ChartInfo>();
					for(long k = fromtime; k <= totime; k+=60000){ 
						ChartInfo cinfo = new ChartInfo();
						String dateString = sourceFormat.format(new Date(k));
						SimpleDateFormat format = new SimpleDateFormat("HH:mm");
						String cutime = format.format(new Date(k));
						cinfo.dt = dateString;
						
						cinfo.crate = "-1";
						String dataString = temp.get(0).dt;
						if(dateString.equals(dataString)){
							break;
						}
						temp1.add(cinfo);
					}
					temp1.addAll(temp);
					boolean ff = true;
					for(long k = fromtime; k <= totime; k+=60000){ 
						ChartInfo cinfo = new ChartInfo();
						String dateString = sourceFormat.format(new Date(k));
						SimpleDateFormat format = new SimpleDateFormat("HH:mm");
						String cutime = format.format(new Date(k));
						cinfo.dt = dateString;
						cinfo.crate = "-1";
						if(!ff){
							temp1.add(cinfo);
						}
						if(dateString.equals(temp.get(temp.size()-1).dt)){
							ff = false;
						}
					}
					temp.clear();
					temp.addAll(temp1);
					
					data = new GraphViewData[temp.size()];
					*/
					/*List<ChartInfo> temp1 = new ArrayList<ChartInfo>();
					if(temp.size() > 500){
						int di = temp.size() / 500;
						if(di == 0)di = 1;
						for(int i = 0; i < temp.size(); i++){
							if(i % di == 0){
								temp1.add(temp.get(i));
							}
						}
					}
					temp.clear();
					temp.addAll(temp1);
					*/
					double maxval = 0;
					double minval = 100000000;
					for (int i=0; i<temp.size(); i++) {
						data[i] = new GraphViewData(i, Double.parseDouble(temp.get(i).crate)); // next day
						
							if(maxval < Double.parseDouble(temp.get(i).crate)){
								maxval = Double.parseDouble(temp.get(i).crate);
							}
							if(minval > Double.parseDouble(temp.get(i).crate)){
								minval = Double.parseDouble(temp.get(i).crate);
							}
					}
					double interval = (maxval - minval) / 4 ;
					maxval = maxval + interval;
					minval = minval - interval;
					interval = (maxval - minval) / 6 ;
					for(int i = 5; i >= 0; i--){
						double a = minval + (5 - i) * interval;
						verlabels[i] = "" + a;
					}
					
					
					int hinterval = temp.size() / 4;
					for(int i = 0; i < 5; i++){
						if(i == 0){
							horlabels[0] = "GMT";
						}else{
							int k = i * hinterval;
							if(k > temp.size()-1)k = temp.size()-1;
							String s = temp.get(k).dt;
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
							Date d;
							try {
								d = simpleDateFormat.parse(s);
					            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
					            s = sdf.format(d);
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								s = "";
							}
				            horlabels[i] = s;
						}
					}
					break;
				case 1:
					maxval = 0;
					minval = 100000000;
					for (int i=0; i<temp.size(); i++) {
						data[i] = new GraphViewData(i, Double.parseDouble(temp.get(i).crate)); // next day
						if(maxval < Double.parseDouble(temp.get(i).crate)){
							maxval = Double.parseDouble(temp.get(i).crate);
						}
						if(minval > Double.parseDouble(temp.get(i).crate)){
							minval = Double.parseDouble(temp.get(i).crate);
						}
					}
					interval = (maxval - minval) / 4 ;
					maxval = maxval + interval;
					minval = minval - interval;
					interval = (maxval - minval) / 6 ;
					for(int i = 5; i >= 0; i--){
						double a = minval + (5 - i) * interval;
						verlabels[i] = "" + a;
					}
					
					hinterval = temp.size() / 4;
					for(int i = 0; i < 5; i++){
							int k = i * hinterval;
							if(k > temp.size()-1)k = temp.size()-1;
							String s = temp.get(k).dt;
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
							Date d;
							try {
								d = simpleDateFormat.parse(s);
					            SimpleDateFormat sdf = new SimpleDateFormat("dd hh");
					            s = sdf.format(d) + "h";
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								s = "";
							}
				            horlabels[i] = s;
					}
					break;
				case 2:
					maxval = 0;
					minval = 100000000;
					for (int i=0; i<temp.size(); i++) {
						data[i] = new GraphViewData(i, Double.parseDouble(temp.get(i).crate)); // next day
						if(maxval < Double.parseDouble(temp.get(i).crate)){
							maxval = Double.parseDouble(temp.get(i).crate);
						}
						if(minval > Double.parseDouble(temp.get(i).crate)){
							minval = Double.parseDouble(temp.get(i).crate);
						}
					}
					interval = (maxval - minval) / 4 ;
					maxval = maxval + interval;
					minval = minval - interval;
					interval = (maxval - minval) / 6 ;
					for(int i = 5; i >= 0; i--){
						double a = minval + (5 - i) * interval;
						verlabels[i] = "" + a;
					}
					
					hinterval = temp.size() / 4;
					for(int i = 0; i < 5; i++){
							int k = i * hinterval;
							if(k > temp.size()-1)k = temp.size()-1;
							String s = temp.get(k).dt;
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
							Date d;
							try {
								d = simpleDateFormat.parse(s);
					            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
					            s = sdf.format(d);
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								s = "";
							}
				            horlabels[i] = s;
					}
					break;
				case 3:
					maxval = 0;
					minval = 100000000;
					for (int i=0; i<temp.size(); i++) {
						data[i] = new GraphViewData(i, Double.parseDouble(temp.get(i).crate)); // next day
						if(maxval < Double.parseDouble(temp.get(i).crate)){
							maxval = Double.parseDouble(temp.get(i).crate);
						}
						if(minval > Double.parseDouble(temp.get(i).crate)){
							minval = Double.parseDouble(temp.get(i).crate);
						}
					}
					interval = (maxval - minval) / 4 ;
					maxval = maxval + interval;
					minval = minval - interval;
					interval = (maxval - minval) / 6 ;
					for(int i = 5; i >= 0; i--){
						double a = minval + (5 - i) * interval;
						verlabels[i] = "" + a;
					}
	
					hinterval = temp.size() / 4;
					for(int i = 0; i < 5; i++){
							int k = i * hinterval;
							if(k > temp.size()-1)k = temp.size()-1;
							String s = temp.get(k).dt;
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
							Date d;
							try {
								d = simpleDateFormat.parse(s);
					            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
					            s = sdf.format(d);
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								s = "";
							}
				            horlabels[i] = s;
					}
					break;
				case 4:
					maxval = 0;
					minval = 100000000;
					for (int i=0; i<temp.size(); i++) {
						data[i] = new GraphViewData(i, Double.parseDouble(temp.get(i).crate)); // next day
						if(maxval < Double.parseDouble(temp.get(i).crate)){
							maxval = Double.parseDouble(temp.get(i).crate);
						}
						if(minval > Double.parseDouble(temp.get(i).crate)){
							minval = Double.parseDouble(temp.get(i).crate);
						}
					}
					interval = (maxval - minval) / 4 ;
					maxval = maxval + interval;
					minval = minval - interval;
					interval = (maxval - minval) / 6 ;
					for(int i = 5; i >= 0; i--){
						double a = minval + (5 - i) * interval;
						verlabels[i] = "" + a;
					}
	
					hinterval = temp.size() / 4;
					for(int i = 0; i < 5; i++){
							int k = i * hinterval;
							if(k > temp.size()-1)k = temp.size()-1;
							String s = temp.get(k).dt;
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
							Date d;
							try {
								d = simpleDateFormat.parse(s);
					            SimpleDateFormat sdf = new SimpleDateFormat("MMM yyyy");
					            s = sdf.format(d);
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								s = "";
							}
				            horlabels[i] = s;
					}
					break;
				case 5:
					maxval = 0;
					minval = 100000000;
					for (int i=0; i<temp.size(); i++) {
						data[i] = new GraphViewData(i, Double.parseDouble(temp.get(i).crate)); // next day
						if(maxval < Double.parseDouble(temp.get(i).crate)){
							maxval = Double.parseDouble(temp.get(i).crate);
						}
						if(minval > Double.parseDouble(temp.get(i).crate)){
							minval = Double.parseDouble(temp.get(i).crate);
						}
					}
					interval = (maxval - minval) / 4 ;
					maxval = maxval + interval;
					minval = minval - interval;
					interval = (maxval - minval) / 6 ;
					for(int i = 5; i >= 0; i--){
						double a = minval + (5 - i) * interval;
						verlabels[i] = "" + a;
					}
					hinterval = temp.size() / 4;
					for(int i = 0; i < 5; i++){
							int k = i * hinterval;
							if(k > temp.size()-1)k = temp.size()-1;
							String s = temp.get(k).dt;
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
							Date d;
							try {
								d = simpleDateFormat.parse(s);
					            SimpleDateFormat sdf = new SimpleDateFormat("MMM yyyy");
					            s = sdf.format(d);
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								s = "";
							}
				            horlabels[i] = s;
					}
					break;
				case 6:
					maxval = 0;
					minval = 100000000;
					for (int i=0; i<temp.size(); i++) {
						data[i] = new GraphViewData(i, Double.parseDouble(temp.get(i).crate)); // next day
						if(maxval < Double.parseDouble(temp.get(i).crate)){
							maxval = Double.parseDouble(temp.get(i).crate);
						}
						if(minval > Double.parseDouble(temp.get(i).crate)){
							minval = Double.parseDouble(temp.get(i).crate);
						}
					}
					interval = (maxval - minval) / 4 ;
					maxval = maxval + interval;
					minval = minval - interval;
					interval = (maxval - minval) / 6 ;
					for(int i = 5; i >= 0; i--){
						double a = minval + (5 - i) * interval;
						verlabels[i] = "" + a;
					}
					
					hinterval = temp.size() / 4;
					for(int i = 0; i < 5; i++){
							int k = i * hinterval;
							if(k > temp.size()-1)k = temp.size()-1;
							String s = temp.get(k).dt;
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
							Date d;
							try {
								d = simpleDateFormat.parse(s);
					            SimpleDateFormat sdf = new SimpleDateFormat("MMM yyyy");
					            s = sdf.format(d);
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								s = "";
							}
				            horlabels[i] = s;
					}
					break;
				}
				String ss = "";
				boolean cflg2 = false;
				for(int i = 0; i < verlabels.length; i++){
					int index  = 0;
					if((index = verlabels[i].indexOf(".")) != -1){
						String a = verlabels[i].substring(0, index);
						String b = verlabels[i].substring(index,verlabels[i].length());
						if(b.length() > 3){
							b = b.substring(0,3);
						}
						verlabels1[i] = a + b;
						if(verlabels1[i].equals(ss)){
							cflg2 = true; break;
						}
						ss = verlabels1[i];
					}
				}
				int cutnum = 3;
				if(cflg2){
					cutnum = 4;
					cflg2 = false;
					for(int i = 0; i < verlabels.length; i++){
						int index  = 0;
						if((index = verlabels[i].indexOf(".")) != -1){
							String a = verlabels[i].substring(0, index);
							String b = verlabels[i].substring(index,verlabels[i].length());
							if(b.length() > cutnum){
								b = b.substring(0,cutnum);
							}
							verlabels1[i] = a + b;
							if(verlabels1[i].equals(ss)){
								cflg2 = true; break;
							}
							ss = verlabels1[i];
						}
					}

					if(cflg2){
						cutnum = 5;
						cflg2 = false;
						for(int i = 0; i < verlabels.length; i++){
							int index  = 0;
							if((index = verlabels[i].indexOf(".")) != -1){
								String a = verlabels[i].substring(0, index);
								String b = verlabels[i].substring(index,verlabels[i].length());
								if(b.length() > cutnum){
									b = b.substring(0,cutnum);
								}
								verlabels[i] = a + b;
							}
						}
					}else{
						verlabels = verlabels1;
					}
				}else{
					verlabels = verlabels1;
				}
				
				GraphViewSeries exampleSeries = new GraphViewSeries(data);
				GraphViewStyle graphstyle = new GraphViewStyle(R.color.chart_text_color, R.color.chart_text_color, R.color.grid_color, R.color.chart_text_color);
				
				graphView = new LineGraphView(CurrencyActivity.this,"", width - 10, height, chartclickbt);
				((LineGraphView) graphView).setDrawBackground(true);
				graphView.addSeries(exampleSeries); // data
				graphView.setVerticalLabels(verlabels);
				graphView.setHorizontalLabels(horlabels);
				if(directflg){
					graphView.setLayoutParams(new LayoutParams(height - 10, (int)((float)width * 5f / 7.0f)));
				}else{
					graphView.setLayoutParams(new LayoutParams(width - 30, (int)((float)height * 5f / 10.0f)));
				}
				//graphView.getGraphViewStyle().setNumHorizontalLabels(5);
				
			}
			return temp;
        }

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			graphLayout.removeAllViews();
			MyTextView a = new MyTextView(CurrencyActivity.this);
			a.setText("Retrieving historical Data...");
			a.setTextSize(13);
			a.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			a.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
			a.setTextColor(getResources().getColor(R.color.date_color));
			graphLayout.addView(a);
			
			
		}
		@Override
		protected void onPostExecute(List<ChartInfo> _result) {
			if(_result == null || _result.size() == 0){
				super.onPostExecute(_result);
				//dialog procee  --- "Network Connection failed. Please check network and try again."
				graphLayout.removeAllViews();
				MyTextView a = new MyTextView(CurrencyActivity.this);
				a.setText("No historical data.");
				a.setTextSize(15);
				a.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
				a.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
				a.setTextColor(getResources().getColor(R.color.date_color));
				graphLayout.addView(a);
			}else{
				super.onPostExecute(_result);
				
				
				//graphView.redrawAll();
				graphLayout.removeAllViews();
				graphLayout.addView(graphView);
				new Thread(){
					public void run(){
						try {
							Thread.sleep(1500);
							CurrencyActivity.this.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									graphView.redrawAll();
								}
							});
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}.start();
				//graphView.redrawAll();
				//graphLayout
				//if(_result != null)
					//((ImageView)findViewById(R.id.chartImage)).setImageBitmap(_result);
			}
	    	  
	    }
    }
	// Wheel scrolled flag
	private boolean wheelScrolled = false;
	// Wheel scrolled listener
	OnWheelScrollListener scrolledListener = new OnWheelScrollListener()
	{
		public void onScrollStarts(WheelView wheel)
			{
				wheelScrolled = true;
			}

		public void onScrollEnds(WheelView wheel)
			{
				wheelScrolled = false;
				updateStatus();
			}

		@Override
		public void onScrollingStarted(WheelView wheel) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onScrollingFinished(WheelView wheel) {
			// TODO Auto-generated method stub
			
		}
	};
	// Wheel changed listener
	private final OnWheelChangedListener changedListener = new OnWheelChangedListener()
	{
		public void onChanged(WheelView wheel, int oldValue, int newValue)
			{
				if (!wheelScrolled)
					{
						updateStatus();
					}
			}
	};
	/**
	 * Updates entered PIN status
	 */
	private void updateStatus()
	{
		if(startflg){
			valinfo.setSelectedLeft(getWheel(R.id.basicList1).getCurrentItem());
			valinfo.setSelectedRight(getWheel(R.id.otherList1).getCurrentItem());
		}
		startflg = true;
		//((MyTextView)((View)getWheel(R.id.otherList1).getTag(getWheel(R.id.otherList1).getCurrentItem())).findViewById(R.id.cuCode)).setTextColor(CurrencyActivity.this.getResources().getColor(R.color.cu_color));
		changeTopData();
		
		//toprightcountry.setGravity(Gravity.RIGHT);
		//toprightcountry.setWidth(LayoutParams.WRAP_CONTENT);
		/*text1.setText(wheelMenu1[getWheel(R.id.p1).getCurrentItem()]);
		text2.setText(wheelMenu2[getWheel(R.id.p2).getCurrentItem()]);
		text3.setText(wheelMenu3[getWheel(R.id.p3).getCurrentItem()]);

		text.setText(wheelMenu1[getWheel(R.id.p1).getCurrentItem()] + " - " + wheelMenu2[getWheel(R.id.p2).getCurrentItem()] + " - " + wheelMenu3[getWheel(R.id.p3).getCurrentItem()]);
		*/
	}
	public void initUI(int value){
		//((ImageView)findViewById(R.id.imageView1)).setVisibility(value);
		topleftflag.setVisibility(value);
		topleftcode.setVisibility(value);
		topleftcountry.setVisibility(value);
		toprightflag.setVisibility(value);
		toprightcode.setVisibility(value);
		toprightcountry.setVisibility(value);
		leftnum.setVisibility(value);
		rightnum.setVisibility(value);
		leftvalue.setVisibility(value);
		rightvalue.setVisibility(value);
		//mainbt.setVisibility(value);
		//calbt.setVisibility(value);
		//graphbt.setVisibility(value);
		//favobt.setVisibility(value);
		bottomFlipper.setVisibility(value);
		((LinearLayout)findViewById(R.id.blineLayout)).setVisibility(value);
		if(value == View.GONE){
			bottomLayout.setBackgroundResource(R.drawable.start_screen_logo);
		}else{
			bottomLayout.setBackgroundResource(R.drawable.main_currency_screen_lists_bg);
		}
		if(value == View.VISIBLE){
			refreshbt.setOnClickListener(this);
			calbt.setOnClickListener(this);
			mainbt.setOnClickListener(this);
			graphbt.setOnClickListener(this);
			favobt.setOnClickListener(this);

			//if(startflg)
			topRlayout.setOnTouchListener(new View.OnTouchListener() {
				float lastx = 0;
				float lasty = 0;
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					switch(event.getAction()){
					case MotionEvent.ACTION_DOWN:
						lastx = event.getX();
						lasty = event.getY();
						break;
					case MotionEvent.ACTION_UP:
						float x = event.getX();
						float y = event.getY();
						if((lastx < width / 2 - 30 || lastx > width / 2 + 30) && Math.abs(lastx - x) > 70){
							if(Math.abs(lastx - x) > 70){
								changeLeftToRight();
							}
						}else if(lastx < width / 2 - 10 && Math.abs(lastx - x) < 20){
							if(lasty < y - 5){
								//left scroll down
								leftScrollDownUp(1);
							}else if(lasty > y + 5){
								//left scroll up
								leftScrollDownUp(-1);
							}
						}else if(lastx > width / 2 + 10 && Math.abs(lastx - x) < 20){
							if(lasty < y - 5){
								//right scroll down
								rightScrollDownUp(1);
							}else if(lasty > y + 5){
								//right scroll up
								rightScrollDownUp(-1);
							}
						}
						break;
					}
					return true;
				}
			});
				
		}
	}
	public synchronized void changeTopData(){
		topleftflag.setVisibility(View.VISIBLE);
		toprightflag.setVisibility(View.VISIBLE);
		topleftcode.setVisibility(View.VISIBLE);
		topleftcountry.setVisibility(View.VISIBLE);
		toprightcode.setVisibility(View.VISIBLE);
		toprightcountry.setVisibility(View.VISIBLE);
		leftnum.setVisibility(View.VISIBLE);
		rightnum.setVisibility(View.VISIBLE);
		leftvalue.setVisibility(View.VISIBLE);
		rightvalue.setVisibility(View.VISIBLE);
		centervalue.setVisibility(View.VISIBLE);
		
		topleftflag.setImageResource(countryflags.getFlags(favolist.get(valinfo.getSelectedLeft()).getCuFlags()));
		toprightflag.setImageResource(countryflags.getFlags(templist.get(valinfo.getSelectedRight()).getCuFlags()));
		topleftcode.setText(favolist.get(valinfo.getSelectedLeft()).getCuCode());
		topleftcountry.setText(favolist.get(valinfo.getSelectedLeft()).getCuDetail());
		String a = favolist.get(valinfo.getSelectedLeft()).getCuDetail();
		if(a.length() > 17){
			topleftcountry.setTextSize(10);
		}else{
			topleftcountry.setTextSize(13);
		}
		toprightcode.setText(templist.get(valinfo.getSelectedRight()).getCuCode());
		toprightcountry.setText(templist.get(valinfo.getSelectedRight()).getCuDetail());
		a = templist.get(valinfo.getSelectedRight()).getCuDetail();
		if(a.length() > 17){
			toprightcountry.setTextSize(10);
		}else{
			toprightcountry.setTextSize(13);
		}
		calculateFunc();
		Date currentTime = new Date();		
		SimpleDateFormat df = new SimpleDateFormat("hh:mm aa");
		String strdate = df.format(currentTime);
		valinfo.setStrDate(strdate);
		long now = System.currentTimeMillis();
		valinfo.setTime(now);
		centervalue.setText(valinfo.getStrDate());

	}
	public String numberProcessing(String num){
		try{
			String num1 = "";
			String[] array1 = num.split("E");
			String[] array;
			int index = 0;
			if((index = array1[0].indexOf("."))!=-1){
				array  = new String[]{array1[0].substring(0, index), array1[0].substring(index+1, array1[0].length())};
			}else{
				array  = new String[]{array1[0].substring(0, array1[0].length())};
			}
			if(array.length > 1){
				num1 = array[0];
				if(array[1].length() > Integer.parseInt(array1[1])){
					num1 = num1 + array[1].substring(0, Integer.parseInt(array1[1]));
					num1 = num1 + "." + array[1].substring(Integer.parseInt(array1[1]), array[1].length());
				}else{
					num1 = num1 + array[1];
					for(int i = 0; i < Integer.parseInt(array1[1]) - array[1].length(); i++){
						num1 = num1 + "0";
					}
				}
			}else if(array.length == 0){
				num1 = array1[0];
				for(int i = 0; i < Integer.parseInt(array1[1]); i++){
					num1 = num1 + "0";
				}
			}
			return num1;
		}catch(Exception e){
			return num;
		}
	}
	public void calculateFunc(){

		String leftnum1 = valinfo.getLeftNum();
		String rightnum1 = "";
		String leftval1 = "";
		String rightval1 = "";
		int fontsize = 25;
		try{
			double lnum = Double.parseDouble(valinfo.getLeftNum());
			String lrate = "0";
			String rrate = "0";
			String leftcode = favolist.get(valinfo.getSelectedLeft()).getCuCode();
			String rightcode = templist.get(valinfo.getSelectedRight()).getCuCode();
			for(int i = 0; i < currencylist.size(); i++){
				if(leftcode.equals(currencylist.get(i).getCuCode())){
					lrate = valinfo.getCrate(i);
				}
				if(rightcode.equals(currencylist.get(i).getCuCode())){
					rrate = valinfo.getCrate(i);
				}
			}
			double lrate1 =  Double.parseDouble(lrate);
			double rrate1 =  Double.parseDouble(rrate);
			double rnum = 0;
			if(lrate1 != 0){
				rnum = lnum * rrate1 / lrate1;
			}else{
				rnum = 0;
			}
			rightnum1 = "" + rnum;
			if(rightnum1.contains("E")){
				rightnum1 = numberProcessing(rightnum1);
			}
			String rightnum2 = "";
			String rightnum3 = "";
			int index = 0;
			if((index = rightnum1.indexOf(".")) != -1){
				rightnum2 = rightnum1.substring(0, index);
				rightnum3 = rightnum1.substring(index+1, rightnum1.length());
				if(rightnum3.length() > 2){
					rightnum3 = rightnum3.substring(0, 2);
				}
				rightnum1 = rightnum2 +"."+rightnum3;
			}
			if(rightnum1.length() > 22){
				rightnum1 = rightnum1.substring(0, 22);
			}
			index = 0;
			String leftnum2 = "";
			String leftnum3 = "";
			if((index = leftnum1.indexOf(".")) != -1){
				leftnum2 = leftnum1.substring(0, index);
				leftnum3 = leftnum1.substring(index+1, leftnum1.length());
				if(leftnum3.length() > 2){
					leftnum3 = leftnum3.substring(0, 2);
				}
				leftnum1 = leftnum2 +"."+leftnum3;
			}
			if(leftnum1.length() < rightnum1.length()){
				if(rightnum1.length() > 15){
					fontsize = 25 - (rightnum1.length() - 5) ;					
				}else if(rightnum1.length() > 7){
					fontsize = 25 - (rightnum1.length() - 7) ;					
				}
			}else{
				if(leftnum1.length() > 15){
					fontsize = 25 - (leftnum1.length() - 5) ;					
				}else if(leftnum1.length() > 7){
					fontsize = 25 - (leftnum1.length() - 7) ;					
				}
			}
			if(fontsize < 11) fontsize = 11;
			double lval = 0;
			if(lrate1 != 0){
				lval = 1.0 * rrate1 / lrate1;
			}else{
				lval = 0;
			}
			leftval1 = "" + lval;
			String leftval2 = "";
			String leftval3 = "";
			index = 0;
			if((index = leftval1.indexOf(".")) != -1){
				leftval2 = leftval1.substring(0, index);
				leftval3 = leftval1.substring(index+1, leftval1.length());
				if(leftval3.length() > 4){
					leftval3 = leftval3.substring(0, 4);
				}
				leftval1 = leftval2 +"."+leftval3;
			}
			

			double rval = 0;
			if(rrate1 != 0){
				rval = 1.0 * lrate1 / rrate1;
			}else{
				rval = 0;
			}
			rightval1 = "" + rval;
			String rightval2 = "";
			String rightval3 = "";
			index = 0;
			if((index = rightval1.indexOf(".")) != -1){
				rightval2 = rightval1.substring(0, index);
				rightval3 = rightval1.substring(index+1, rightval1.length());
				if(rightval3.length() > 4){
					rightval3 = rightval3.substring(0, 4);
				}
				rightval1 = rightval2 +"."+rightval3;
			}
			rightValue = rightval1;
			leftval1 = favolist.get(valinfo.getSelectedLeft()).getCuSymbol() + " 1 = " + templist.get(valinfo.getSelectedRight()).getCuSymbol() + " " + leftval1;
			rightval1 = templist.get(valinfo.getSelectedRight()).getCuSymbol() + " 1 = " + favolist.get(valinfo.getSelectedLeft()).getCuSymbol() + " " + rightval1;
			
		}catch(Exception e){
			rightnum1 = "0.0";	
			rightValue = "0.0";
			leftval1 = favolist.get(valinfo.getSelectedLeft()).getCuSymbol() + " 1 = " + templist.get(valinfo.getSelectedRight()).getCuSymbol() + " 0.0";
			rightval1 = templist.get(valinfo.getSelectedRight()).getCuSymbol() + " 1 = " + favolist.get(valinfo.getSelectedLeft()).getCuSymbol() + " 0.0";
		}
		leftnum.setTextSize((float)fontsize);
		leftnum.setText(leftnum1);
		if(rightnum1.equals("0.0"))
			rightnum1 = "0";
		rightnum.setTextSize((float)fontsize);
		rightnum.setText(rightnum1);

		leftvalue.setText(leftval1);
		rightvalue.setText(rightval1);
	}
	 @Override
    public void onBackPressed() {
		 if (this.lastBackPressTime < System.currentTimeMillis() - 3000) {
			this.lastBackPressTime = System.currentTimeMillis();
			toast = Toast.makeText(this, "Press back again to close this app", 3000);
			toast.show();
		} else {
			if (toast != null)
				toast.cancel();
			valinfo.setThreadFlg(false);
			finish();
		}
    }
	 @Override
	 protected void onPause() {
		 valinfo.setThreadFlg(false);
		 super.onPause();
	 }
	 @Override
	 protected void onResume() {
		 // TODO Auto-generated method stub
		if(!valinfo.getThreadFlg()){
			valinfo.setThreadFlg(true);
			new GetThread().start();
		}
		FavoStore fstore = new FavoStore(CurrencyActivity.this);
		int[] flist = fstore.getFavourites();
		favostorelist.clear();
		for(int i = 0; i < currencylist.size(); i++){
			for(int j = 0; j < flist.length; j++){
				if(i == flist[j]){
					favostorelist.add(currencylist.get(i));
					break;
				}
			}
        }
		super.onResume();
	 }
	 class GetThread extends Thread {
		    @Override
		    public void run() {
		        // do some background processing
		    	try{
			    	while(valinfo.getThreadFlg()){
						if(con.checkNextworkConnection(CurrencyActivity.this)){
				    		String ratestring = "";
							for(int i = 0; i < codes.length; i++){
								if(i == 0){
									ratestring = "USD" + codes[i] + "=X,";
								}else if(i == codes.length - 1){
									ratestring = ratestring + "USD" + codes[i] + "=X";
								}else{
									ratestring = ratestring + "USD" + codes[i] + "=X,";
								}
							}
							CurrencyActivity.this.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									centervalue.setText("Updating...");
								}
							});
							
							con.GetRatesFromYahooService(CurrencyActivity.this, ratestring, valinfo);
							
							CurrencyActivity.this.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									changeTopData();
								}
							});	
						}else{
							CurrencyActivity.this.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									//valinfo.setThreadFlg(false);
									centervalue.setText("Not Connection");
									//MyAlertDialog dialog = new MyAlertDialog(CurrencyActivity.this);
									//dialog.show();
								}
							});
						}
						sleep(60000);
			    	}
		    	}catch(Exception e){
		    		e.getStackTrace();
		    	}finally{
		    		
		    	}
		    }
	};
}
