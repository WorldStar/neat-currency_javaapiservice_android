package com.matt.currency;

public class FavoInfo {
	String cucode;
	String cuDetail;
	String cuFlags;
	String cuSymbol;
	public void setCuDetail(String details){
		this.cuDetail = details;
	}
	public String getCuDetail(){
		return cuDetail;
	}
	public void setCuCode(String code){
		this.cucode = code;
	}
	public String getCuCode(){
		return cucode;
	}
	public void setCuFlags(String image){
		this.cuFlags = image;
	}
	public String getCuFlags(){
		return cuFlags;
	}
	public void setCuSymbol(String symbol){
		this.cuSymbol = symbol;
	}
	public String getCuSymbol(){
		return cuSymbol;
	}
}
