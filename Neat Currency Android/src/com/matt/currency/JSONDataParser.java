package com.matt.currency;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONDataParser {
	
	public List<CurrencyInfo> currencyParserFromStringData(String[] codes, String[] details, String[] symbols,FlagInfo flags){
		List<CurrencyInfo> culist = new ArrayList<CurrencyInfo>();
		try {
			for(int i = 0; i < codes.length; i++){
				CurrencyInfo cuinfo = new CurrencyInfo();
				cuinfo.setCuCode(codes[i]);
				cuinfo.setCuDetail(details[i]);
				cuinfo.setCuFlags(i);
				cuinfo.setCuSymbol(symbols[i]);
				culist.add(cuinfo);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.toString());
			culist.clear();
		}
		return culist;
	}
	public List<CurrencyInfo> currencyParserFromStringData1(int[] flist, String[] codes, String[] details, String[] symbols,FlagInfo flags){
		List<CurrencyInfo> culist = new ArrayList<CurrencyInfo>();
		try {
			for(int i = 0; i < flist.length; i++){
				CurrencyInfo cuinfo = new CurrencyInfo();
				cuinfo.setCuCode(codes[flist[i]]);
				cuinfo.setCuDetail(details[flist[i]]);
				cuinfo.setCuFlags(flist[i]);
				cuinfo.setCuSymbol(symbols[flist[i]]);
				culist.add(cuinfo);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.toString());
			culist.clear();
		}
		return culist;
	}
}
class Utils{
    public static SortedMap<Currency, Locale> currencyLocaleMap;
    static {
        currencyLocaleMap = new TreeMap<Currency, Locale>(new Comparator<Currency>() {
          public int compare(Currency c1, Currency c2){
              return c1.getCurrencyCode().compareTo(c2.getCurrencyCode());
          }
      });
      for (Locale locale : Locale.getAvailableLocales()) {
           try {
               Currency currency = Currency.getInstance(locale);
           currencyLocaleMap.put(currency, locale);
           }catch (Exception e){
       }
      }
  }

  public static String getCurrencySymbol(String currencyCode) {
      Currency currency = Currency.getInstance(currencyCode);
      System.out.println( currencyCode+ ":-" + currency.getSymbol(currencyLocaleMap.get(currency)));
      return currency.getSymbol(currencyLocaleMap.get(currency));
  }
}
