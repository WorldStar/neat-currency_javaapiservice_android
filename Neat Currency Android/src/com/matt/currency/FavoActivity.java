package com.matt.currency;

import static com.nineoldandroids.view.ViewPropertyAnimator.animate;

import java.util.ArrayList;
import java.util.List;

import com.matt.currency.utils.MyButton;
import com.matt.currency.utils.MyTextView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

public class FavoActivity  extends Activity implements OnClickListener, OnItemClickListener{
	MyButton backbt, addbt;
	
	LinearLayout addlayout;
	ListView listview;
	List<CurrencyInfo> favolist = new ArrayList<CurrencyInfo>();
	List<ResourceInfo> resourcelist = new ArrayList<ResourceInfo>();
	FlagInfo countryflags = new FlagInfo();
	FavoAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.favorite);
		addlayout = (LinearLayout)findViewById(R.id.addLayout);
		backbt = (MyButton)findViewById(R.id.backbt);
		addbt = (MyButton)findViewById(R.id.addbt);
		listview = (ListView)findViewById(R.id.listView1);
		listview.setCacheColorHint(Color.TRANSPARENT);
		addlayout.setOnClickListener(this);
		backbt.setOnClickListener(this);
		addbt.setOnClickListener(this);
		//loadListData();
	}
	@Override
	public void onResume(){
		favolist.clear();
		resourcelist.clear();
		FavoStore fstore = new FavoStore(FavoActivity.this);
		int[] flist = fstore.getFavourites();
		JSONDataParser parser = new JSONDataParser();
		String[] flags;
		String[] codes;
		String[] details;
		String[] symbols;
		flags = getResources().getStringArray(R.array.currency_flags);
		codes = getResources().getStringArray(R.array.currency_codes);
		details = getResources().getStringArray(R.array.currency_details);
		symbols = getResources().getStringArray(R.array.corrency_symbols);
		favolist = parser.currencyParserFromStringData1(flist, codes,details,symbols, countryflags);
		for(int i = 0; i < favolist.size(); i++){
			ResourceInfo reinfo = new ResourceInfo();
			reinfo.setRemoveId(0);
			resourcelist.add(reinfo);
		}
		adapter=new FavoAdapter(this, R.layout.favoscreen_list, favolist);
		
		listview.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		listview.setSmoothScrollbarEnabled(true);
		listview.setOnItemClickListener(this);
		super.onResume();
	}
	private void loadListData(){
		
		FavoStore fstore = new FavoStore(FavoActivity.this);
		int[] flist = fstore.getFavourites();
		JSONDataParser parser = new JSONDataParser();
		String[] flags;
		String[] codes;
		String[] details;
		String[] symbols;
		flags = getResources().getStringArray(R.array.currency_flags);
		codes = getResources().getStringArray(R.array.currency_codes);
		details = getResources().getStringArray(R.array.currency_details);
		symbols = getResources().getStringArray(R.array.corrency_symbols);
		favolist = parser.currencyParserFromStringData1(flist, codes,details,symbols, countryflags);
		for(int i = 0; i < favolist.size(); i++){
			ResourceInfo reinfo = new ResourceInfo();
			reinfo.setRemoveId(0);
			resourcelist.add(reinfo);
		}
		adapter=new FavoAdapter(this, R.layout.favoscreen_list, favolist);
		
		listview.setAdapter(adapter);
		listview.setSmoothScrollbarEnabled(true);
		listview.setOnItemClickListener(this);
		adapter.notifyDataSetChanged();
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.addLayout:
			Intent intent = new Intent(FavoActivity.this, AddFavoActivity.class);
			FavoActivity.this.startActivity(intent);
			overridePendingTransition(R.anim.anim_bottom_in, R.anim.anim_alpha_out);
			break;
		case R.id.addbt:
			intent = new Intent(FavoActivity.this, AddFavoActivity.class);
			FavoActivity.this.startActivity(intent);
			overridePendingTransition(R.anim.anim_bottom_in, R.anim.anim_alpha_out);
			break;
		case R.id.backbt:
			onBackPressed();
			break;
		}
	}
	@Override
	public void onBackPressed() {
	    super.onBackPressed();
		overridePendingTransition(0, R.anim.anim_top_out);
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
		// TODO Auto-generated method stub
	}
	public class FavoAdapter extends BaseAdapter 
	{
		private List<CurrencyInfo>items;
		public FavoAdapter(Context context, int textViewResourceId,
				List<CurrencyInfo> favolist) {
			
			this.items=favolist;
			// TODO Auto-generated constructor stub
		}

		@Override
		public int getCount() {  
	        return items.size();  
	    }  
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View v=convertView;
			final CurrencyInfo item=items.get(position);
			LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if(v==null)
			{
				v = vi.inflate(R.layout.favoscreen_list, null);
			}
			final View view = v;
			final int pos = position;
			v.setBackgroundColor(getResources().getColor(R.color.favo_back_color));
			MyTextView cuCode = (MyTextView)v.findViewById(R.id.cuCode);
			MyTextView cuCountry = (MyTextView)v.findViewById(R.id.cuCountry);
			final ImageView removeimg = (ImageView)v.findViewById(R.id.removeimg);
			final ImageView infoimg = (ImageView)v.findViewById(R.id.infoimg);
			
			final ImageView delimg = (ImageView)v.findViewById(R.id.delimg);
			if(resourcelist.get(pos).getRemoveId() == 0){
				delimg.setVisibility(View.GONE);
				infoimg.setVisibility(View.VISIBLE);
				removeimg.setImageResource(R.drawable.favorite_screen_remove_icon_0);
				
				view.setBackgroundColor(getResources().getColor(R.color.favo_back_color));
			}else{
				delimg.setVisibility(View.VISIBLE);
				infoimg.setVisibility(View.GONE);
				removeimg.setImageResource(R.drawable.favorite_screen_remove_icon_0);
				
				RotateAnimation anim = new RotateAnimation(360f, 270f, removeimg.getWidth() / 2.0f, removeimg.getHeight()/ 2.0f);
				anim.setInterpolator(new LinearInterpolator());
				anim.setRepeatCount(0);
				anim.setDuration(50);
				anim.setFillAfter(true);
				removeimg.startAnimation(anim);
				view.setBackgroundColor(getResources().getColor(R.color.favo_back_color1));
			}
			delimg.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//makeAndShowDialogBox(pos).show();
					String message = "Do you want to delete?";
					CustomizeDialog customizeDialog = new CustomizeDialog(FavoActivity.this, message, pos, 1);
					customizeDialog.show();
				}
			});
			removeimg.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v1) {
					// TODO Auto-generated method stub
					if(resourcelist.get(pos).getRemoveId() == 0){
						RotateAnimation anim = new RotateAnimation(360f, 270f, removeimg.getWidth() / 2.0f, removeimg.getHeight()/ 2.0f);
						anim.setInterpolator(new LinearInterpolator());
						anim.setRepeatCount(0);
						anim.setDuration(200);
						anim.setFillAfter(true);
						removeimg.startAnimation(anim);
						animate(delimg).alpha(1);
						infoimg.setVisibility(View.GONE);
						delimg.setVisibility(View.VISIBLE);
						//infoimg.setImageResource(R.drawable.aa_favo_delete_color);
						//removeimg.setImageResource(R.drawable.favorite_screen_remove_icon_1);
						view.setBackgroundColor(getResources().getColor(R.color.favo_back_color1));
						resourcelist.get(pos).setRemoveId(1);
					}else{
						//infoimg.setImageResource(R.drawable.favorite_screen_info_icon);

						RotateAnimation anim = new RotateAnimation(270f, 360f, removeimg.getWidth() / 2.0f, removeimg.getHeight()/ 2.0f);
						anim.setInterpolator(new LinearInterpolator());
						anim.setRepeatCount(0);
						anim.setDuration(200);
						anim.setFillAfter(true);
						removeimg.startAnimation(anim);
						animate(delimg).alpha(0);
						delimg.setVisibility(View.GONE);
						infoimg.setVisibility(View.VISIBLE);
						//removeimg.setImageResource(R.drawable.favorite_screen_remove_icon_0);
						view.setBackgroundColor(getResources().getColor(R.color.favo_back_color));
						resourcelist.get(pos).setRemoveId(0);
					}
				}
			});
			ImageView flagimg = (ImageView)v.findViewById(R.id.flgimg);
			cuCode.setText(item.getCuCode());
			cuCountry.setText(item.getCuDetail());
			flagimg.setImageResource(countryflags.getFlags(item.getCuFlags()));
			v.setTag("" + position);
			return v;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}
		
	}
	public void DeleteProcessing(int pos){
		int p = pos;
		List<CurrencyInfo> favolist1 = new ArrayList<CurrencyInfo>();
		List<ResourceInfo> resourcelist1 = new ArrayList<ResourceInfo>();
		for(int i = 0; i < favolist.size() ; i++){
			if(i != p){
				favolist1.add(favolist.get(i));
				resourcelist.get(i).setRemoveId(0);
				resourcelist1.add(resourcelist.get(i));
			}
		}
		favolist.clear();
		favolist.addAll(favolist1);
		resourcelist.clear();
		resourcelist.addAll(resourcelist1);
		adapter=new FavoAdapter(FavoActivity.this, R.layout.favoscreen_list, favolist);
		
		listview.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		FavoStore fstore = new FavoStore(FavoActivity.this);
		String data = "";
		if(favolist.size() > 0){
			data = "" + favolist.get(0).getCuFlags();
			for(int i = 1; i < favolist.size(); i++){
				data = data + ","+ favolist.get(i).getCuFlags();
			}
		}else{
			data = "";
		}
		fstore.saveFavourites(data);
	}
	private AlertDialog makeAndShowDialogBox(int pos){
    	final int p = pos;
        AlertDialog myQuittingDialogBox = 

        	new AlertDialog.Builder(this) 
        	//set message, title, and icon
        	.setTitle("Neat Currency") 
        	.setMessage("Are you sure that you want to delete?")
        	.setIcon(R.drawable.dialog_icon)
        	
        	//set three option buttons
        	.setPositiveButton("Yes", new DialogInterface.OnClickListener() { 
        		public void onClick(DialogInterface dialog, int whichButton) { 
            	 //whatever should be done when answering "YES" goes here

        			List<CurrencyInfo> favolist1 = new ArrayList<CurrencyInfo>();
        			List<ResourceInfo> resourcelist1 = new ArrayList<ResourceInfo>();
        			for(int i = 0; i < favolist.size() ; i++){
        				if(i != p){
        					favolist1.add(favolist.get(i));
        					resourcelist.get(i).setRemoveId(0);
        					resourcelist1.add(resourcelist.get(i));
        				}
        			}
        			favolist.clear();
        			favolist.addAll(favolist1);
        			resourcelist.clear();
        			resourcelist.addAll(resourcelist1);
        			adapter=new FavoAdapter(FavoActivity.this, R.layout.favoscreen_list, favolist);
        			
        			listview.setAdapter(adapter);
        			adapter.notifyDataSetChanged();
        			FavoStore fstore = new FavoStore(FavoActivity.this);
        			String data = "";
        			if(favolist.size() > 0){
	        			data = "" + favolist.get(0).getCuFlags();
	        			for(int i = 1; i < favolist.size(); i++){
	        				data = data + ","+ favolist.get(i).getCuFlags();
	        			}
        			}else{
        				data = "";
        			}
        			fstore.saveFavourites(data);
        		}              
        	})//setPositiveButton
        	
        	.setNegativeButton("NO", new DialogInterface.OnClickListener() { 
        		public void onClick(DialogInterface dialog, int whichButton) { 
            	 //whatever should be done when answering "NO" goes here
        			dialog.cancel();
        		} 
        	})//setNegativeButton
        	
        	.create();
        	
        	return myQuittingDialogBox;
    }
}

