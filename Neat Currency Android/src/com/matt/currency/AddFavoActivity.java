package com.matt.currency;

import java.util.ArrayList;
import java.util.List;

import com.matt.currency.FavoActivity.FavoAdapter;
import com.matt.currency.utils.MyButton;
import com.matt.currency.utils.MyTextView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class AddFavoActivity  extends Activity implements OnClickListener, OnItemClickListener{
	MyButton backbt, addbt;
	
	LinearLayout addlayout;
	ListView listview;
	List<CurrencyInfo> culist = new ArrayList<CurrencyInfo>();
	List<CurrencyInfo> templist = new ArrayList<CurrencyInfo>();
	List<CurrencyInfo> fvlist = new ArrayList<CurrencyInfo>();
	List<ResourceInfo> resourcelist = new ArrayList<ResourceInfo>();
	List<ResourceInfo> rtemplist = new ArrayList<ResourceInfo>();
	FlagInfo countryflags = new FlagInfo();
	CurrencyAdapter adapter;
	EditText search;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.addfavorite);
		backbt = (MyButton)findViewById(R.id.backbt);
		addbt = (MyButton)findViewById(R.id.addbt);
		listview = (ListView)findViewById(R.id.listView1);
		listview.setCacheColorHint(Color.TRANSPARENT);
		search = (EditText)findViewById(R.id.editText1);
		search.clearFocus();
		search.addTextChangedListener(new TextWatcher(){

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub			
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				realSearchChanged(search.getText().toString());	
				
			}
			
		});
		backbt.setOnClickListener(this);
		addbt.setOnClickListener(this);
		loadListData();
	}
	public void realSearchChanged(String value){
		List<CurrencyInfo> culist1 = new ArrayList<CurrencyInfo>();
		List<ResourceInfo> resourcelist1 = new ArrayList<ResourceInfo>();
		for(int i = 0; i < templist.size() ; i++){
			if(value.equals("") || templist.get(i).getCuCode().toLowerCase().contains(value.toLowerCase()) || templist.get(i).getCuDetail().toLowerCase().contains(value.toLowerCase())){
				culist1.add(templist.get(i));
				rtemplist.get(i).setRemoveId(0);
				resourcelist1.add(rtemplist.get(i));
			}
		}
		culist.clear();
		culist.addAll(culist1);
		resourcelist.clear();
		resourcelist.addAll(resourcelist1);
		adapter=new CurrencyAdapter(AddFavoActivity.this, R.layout.favoscreen_list, culist);
		
		listview.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}
	private void loadListData(){
		FavoStore fstore = new FavoStore(AddFavoActivity.this);
		int[] flist = fstore.getFavourites();
		JSONDataParser parser = new JSONDataParser();
		String[] flags;
		String[] codes;
		String[] details;
		String[] symbols;
		flags = getResources().getStringArray(R.array.currency_flags);
		codes = getResources().getStringArray(R.array.currency_codes);
		details = getResources().getStringArray(R.array.currency_details);
		symbols = getResources().getStringArray(R.array.corrency_symbols);

		List<CurrencyInfo> culist1 = new ArrayList<CurrencyInfo>();
		culist1 = parser.currencyParserFromStringData(codes,details,symbols, countryflags);
		fvlist = parser.currencyParserFromStringData1(flist, codes,details,symbols, countryflags);
		for(int i = 0; i < culist1.size(); i++){
			boolean flg = false;
			for(int j = 0; j < fvlist.size(); j++){
				if(culist1.get(i).getCuFlags() == fvlist.get(j).getCuFlags()){
					flg = true; break;
				}
			}
			if(!flg){
				culist.add(culist1.get(i));
			}
		}
		for(int i = 0; i < culist.size(); i++){
			ResourceInfo reinfo = new ResourceInfo();
			reinfo.setRemoveId(0);
			resourcelist.add(reinfo);
		}
		templist.addAll(culist);
		rtemplist.addAll(resourcelist);
		adapter=new CurrencyAdapter(this, R.layout.cuscreen_list, culist);
		
		listview.setAdapter(adapter);
		listview.setSmoothScrollbarEnabled(true);
		listview.setOnItemClickListener(this);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.addbt:
			boolean flg = false;
			for(int i = 0; i < resourcelist.size() ; i++){
				if(resourcelist.get(i).getRemoveId() == 1){
					flg = true; break;
				}
			}
			if(!flg){
				CustomizeDialog customizeDialog = new CustomizeDialog(AddFavoActivity.this, "Please select currencies.", 0, 3);
				customizeDialog.show();
			}else{
				CustomizeDialog customizeDialog = new CustomizeDialog(AddFavoActivity.this, "Do you want to add the selected currencies?", 0, 2);
				customizeDialog.show();
			}
			break;
		case R.id.backbt:
			onBackPressed();
			finish();
			break;
		}
	}
	@Override
	public void onBackPressed() {
	    super.onBackPressed();
		overridePendingTransition(0, R.anim.anim_top_out);
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
		// TODO Auto-generated method stub
	}
	public class CurrencyAdapter extends BaseAdapter 
	{
		private List<CurrencyInfo>items;
		public CurrencyAdapter(Context context, int textViewResourceId,
				List<CurrencyInfo> culist) {
			
			this.items=culist;
			// TODO Auto-generated constructor stub
		}

		@Override
		public int getCount() {  
	        return items.size();  
	    }  
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View v=convertView;
			final CurrencyInfo item=items.get(position);
			LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if(v==null)
			{
				v = vi.inflate(R.layout.cuscreen_list, null);
			}
			final View view = v;
			final int pos = position;
			v.setBackgroundColor(getResources().getColor(R.color.favo_back_color));
			MyTextView cuCode = (MyTextView)v.findViewById(R.id.cuCode);
			MyTextView cuCountry = (MyTextView)v.findViewById(R.id.cuCountry);
			if(resourcelist.get(pos).getRemoveId() == 0){
				v.setBackgroundColor(getResources().getColor(R.color.favo_back_color));
			}else{
				v.setBackgroundColor(getResources().getColor(R.color.favo_back_color1));
			}
			v.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v1) {
					// TODO Auto-generated method stub
					if(resourcelist.get(pos).getRemoveId() == 0){
						view.setBackgroundColor(getResources().getColor(R.color.favo_back_color1));
						resourcelist.get(pos).setRemoveId(1);
					}else{
						view.setBackgroundColor(getResources().getColor(R.color.favo_back_color));
						resourcelist.get(pos).setRemoveId(0);
					}
				}
			});
			ImageView flagimg = (ImageView)v.findViewById(R.id.flgimg);
			cuCode.setText(item.getCuCode());
			cuCountry.setText(item.getCuDetail());
			flagimg.setImageResource(countryflags.getFlags(item.getCuFlags()));
			v.setTag("" + position);
			return v;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}
		
	}
	public void SaveProcessing(){
		List<CurrencyInfo> culist1 = new ArrayList<CurrencyInfo>();
		List<ResourceInfo> resourcelist1 = new ArrayList<ResourceInfo>();
		for(int i = 0; i < resourcelist.size() ; i++){
			if(resourcelist.get(i).getRemoveId() == 1){
				fvlist.add(culist.get(i));
			}else{
				culist1.add(culist.get(i));
				resourcelist1.add(resourcelist.get(i));
			}
		}
		List<CurrencyInfo> culist2 = new ArrayList<CurrencyInfo>();
		List<ResourceInfo> resourcelist2 = new ArrayList<ResourceInfo>();
		for(int i = 0; i < templist.size() ; i++){
			boolean f = false;
			for(int j = 0; j < fvlist.size(); j++){
				if(fvlist.get(j).getCuFlags() == templist.get(i).getCuFlags()){
					f = true; break;
				}
			}        				
			if(!f){
				culist2.add(templist.get(i));
				resourcelist2.add(rtemplist.get(i));
			}
		}
		templist.clear();
		templist.addAll(culist2);
		rtemplist.clear();
		rtemplist.addAll(resourcelist2);
		
		
		culist.clear();
		culist.addAll(culist1);
		resourcelist.clear();
		resourcelist.addAll(resourcelist1);
		adapter=new CurrencyAdapter(AddFavoActivity.this, R.layout.favoscreen_list, culist);
		
		listview.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		FavoStore fstore = new FavoStore(AddFavoActivity.this);
		String data = "";
		if(fvlist.size() > 0){
			data = "" + fvlist.get(0).getCuFlags();
			for(int i = 1; i < fvlist.size(); i++){
				data = data + ","+ fvlist.get(i).getCuFlags();
			}
		}else{
			data = "";
		}
		fstore.saveFavourites(data);
	}
	private AlertDialog makeAndShowDialogBox(){
        AlertDialog myQuittingDialogBox = 

        	new AlertDialog.Builder(this) 
        	//set message, title, and icon
        	.setTitle("Neat Currency") 
        	.setMessage("Do you want to save the selected currencies?")
        	.setIcon(R.drawable.dialog_icon)
        	
        	//set three option buttons
        	.setPositiveButton("Yes", new DialogInterface.OnClickListener() { 
        		public void onClick(DialogInterface dialog, int whichButton) { 
            	 //whatever should be done when answering "YES" goes here

        			List<CurrencyInfo> culist1 = new ArrayList<CurrencyInfo>();
        			List<ResourceInfo> resourcelist1 = new ArrayList<ResourceInfo>();
        			for(int i = 0; i < resourcelist.size() ; i++){
        				if(resourcelist.get(i).getRemoveId() == 1){
        					fvlist.add(culist.get(i));
        				}else{
        					culist1.add(culist.get(i));
        					resourcelist1.add(resourcelist.get(i));
        				}
        			}
        			List<CurrencyInfo> culist2 = new ArrayList<CurrencyInfo>();
        			List<ResourceInfo> resourcelist2 = new ArrayList<ResourceInfo>();
        			for(int i = 0; i < templist.size() ; i++){
        				boolean f = false;
        				for(int j = 0; j < fvlist.size(); j++){
        					if(fvlist.get(j).getCuFlags() == templist.get(i).getCuFlags()){
        						f = true; break;
        					}
        				}        				
        				if(!f){
        					culist2.add(templist.get(i));
        					resourcelist2.add(rtemplist.get(i));
        				}
        			}
        			templist.clear();
        			templist.addAll(culist2);
        			rtemplist.clear();
        			rtemplist.addAll(resourcelist2);
        			
        			
        			culist.clear();
        			culist.addAll(culist1);
        			resourcelist.clear();
        			resourcelist.addAll(resourcelist1);
        			adapter=new CurrencyAdapter(AddFavoActivity.this, R.layout.favoscreen_list, culist);
        			
        			listview.setAdapter(adapter);
        			adapter.notifyDataSetChanged();
        			FavoStore fstore = new FavoStore(AddFavoActivity.this);
        			String data = "";
        			if(fvlist.size() > 0){
	        			data = "" + fvlist.get(0).getCuFlags();
	        			for(int i = 1; i < fvlist.size(); i++){
	        				data = data + ","+ fvlist.get(i).getCuFlags();
	        			}
        			}else{
        				data = "";
        			}
        			fstore.saveFavourites(data);
        		}              
        	})//setPositiveButton
        	
        	.setNegativeButton("NO", new DialogInterface.OnClickListener() { 
        		public void onClick(DialogInterface dialog, int whichButton) { 
            	 //whatever should be done when answering "NO" goes here
        			dialog.cancel();
        		} 
        	})//setNegativeButton
        	
        	.create();
        	
        	return myQuittingDialogBox;
    }

	private AlertDialog makeShowDialogBox(){
        AlertDialog myQuittingDialogBox = 

        	new AlertDialog.Builder(this) 
        	//set message, title, and icon
        	.setTitle("Neat Currency") 
        	.setMessage("Please select currencies.")
        	.setIcon(R.drawable.dialog_icon)
        	
        	//set three option buttons
        	.setPositiveButton("Close", new DialogInterface.OnClickListener() { 
        		public void onClick(DialogInterface dialog, int whichButton) { 
            	 //whatever should be done when answering "YES" goes here

        			dialog.cancel();
        		}              
        	})//setPositiveButton
        	
        	
        	.create();
        	
        	return myQuittingDialogBox;
    }
}

