package com.matt.currency.utils;


import com.matt.currency.R;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Button;

public class MyAlertDialog extends Dialog implements OnClickListener {

	public enum DIALOG_TYPE
	{
		ALERT, CONFIRM
	}
	Context con;
	int c = 0;
	protected DIALOG_TYPE mDlgType;
	
	protected String m_strDialogTitle;
	protected String m_strDialogContent;
	
	protected boolean mDialogResult;
	
	protected int mDialogID;
	
	public MyAlertDialog(Context context){
		
		super(context, R.style.mydialogstyle);
		this.con = context;		
		m_strDialogTitle = "Neat Currency";
		m_strDialogContent = "Can not connect internet. Please check internet.";
		c = context.getResources().getColor(R.color.white);
		mDialogResult = false;
		
		mDialogID = 0;
	}
	
	public MyAlertDialog(DIALOG_TYPE type, Context context, String title, String content){
	
		super(context, R.style.mydialogstyle);
		
		mDlgType = type;
		m_strDialogTitle = title;
		m_strDialogContent = content;
		
		mDialogResult = false;
		
		mDialogID = 0;
		
	}
	
	public int getDialogID(){
		return mDialogID;
	}
	
	public void setDialogID(int id){
		mDialogID = id;
	}
	
	public void setDialogType(DIALOG_TYPE type){
		mDlgType = type;
	}
	
	public void setDialogTitle(String str){
		m_strDialogTitle = str;
	}
	
	public void setDialogContent(String str){
		m_strDialogContent = str;
	}
	
	
	@Override
	public void onStart(){
		
		this.setContentView(R.layout.customalertdialog);
		
		((TextView)this.findViewById(R.id.alertTitle)).setText(m_strDialogTitle);
		
			((TextView)this.findViewById(R.id.alertMessage)).setText(m_strDialogContent);
		
		View viewNegativeBtn = this.findViewById(R.id.alertNegativeButton);
		
		
		((Button)this.findViewById(R.id.alertNegativeButton)).setOnClickListener(this);
		
		
	}
	
	
	public boolean getDialogResult(){
		return mDialogResult;
	}
	
	public void onClick(View v){
		
		int id = v.getId();
		
		switch(id){
		
			case R.id.alertNegativeButton:
				mDialogResult = false;
				break;
		}
	}
	
}
