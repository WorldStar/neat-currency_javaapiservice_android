

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;



public class cuCheckThread extends Thread{

	/**
	 * @param args
	 */
	//Check thread
	public static cuCheckThread checkthread;
	// get data thread
	public static cuThread cuthread;
	// get data thread flag
	static public boolean checkthreadflg = false;
	// check thread flag
	static boolean startcheckflg = false;
	
	static int lastdate = 0;
	// start flag
	static boolean startflg = false;
	// currencies code array
	public String[] codes = new String[]{"EUR","USD","GBP","CHF","JPY","AUD","CAD","BRL","ILS","CNY","RUB","FJD","MYR","ALL","SZL","BIF","ZMK","HKD","TWD","KPW","ARS","NPR","BYR","SCR","DZD","AMD","AWG","AZN","BSD","BHD","BDT","BBD","BZD","BMD","BTN","BOB","BAM","BWP","BND","BGN","KHR","CVE","KYD","XAF","CLP","COP","KMF","CRC","HRK","CUP","CZK","DKK","DJF","DOP","XCD","EGP","SVC","ERN","ETB","FKP","GMD","GHS","GIP","GTQ","GNF","GYD","HTG","HNL","HUF","ISK","INR","IDR","IRR","IQD","JMD","JOD","KZT","KES","KWD","KGS","LAK","LVL","LBP","LSL","LRD","LYD","LTL","MOP","MKP","MGA","MWK","MVR","MRO","MUR","MXN","MDL","MNT","MAD","MZN","MMK","NAD","ANG","NZD","NIO","NGN","NOK","OMR","XPF","PKR","PAB","PGK","PYG","PEN","PHP","PLN","QAR","RON","RWF","WST","STD","SAR","RSD","SLL","SGD","SKK","LKR","SBD","SOS","ZAR","KRW","SHP","SDG","SRD","SEK","SYP","THB","TZS","TOP","TTD","TND","TRY","UGX","UAH","AED","UYU","VUV","VEF","VND","XOF","YER","ZWD","XAU","XAG","XPT"};
	// main function
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		checkthreadflg = false;
		// check for check thread
		checkthread = _Instance();
		if(checkthread != null){
			checkthread.start();
			Calendar c = Calendar.getInstance();
			setLog(c.get(Calendar.YEAR)+":"+c.get(Calendar.MONTH)+":"+c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.HOUR)+":"+c.get(Calendar.MINUTE)+": Monitor Thread Starting\r\n");
		}else{
			System.out.println("OK");Calendar c = Calendar.getInstance();
			setLog(c.get(Calendar.YEAR)+":"+c.get(Calendar.MONTH)+":"+c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.HOUR)+":"+c.get(Calendar.MINUTE)+": Already Monitor Thread started.\r\n");
		}
	      
	}
	//check thread instance func
	public static cuCheckThread _Instance(){
		if(checkthread == null){
			checkthread = new cuCheckThread();
			startcheckflg = true;
		}else{
			return null;
		}
		return checkthread;
	}
	//get data thread instance func
	public cuThread Instance(){
		if(cuthread == null){
			cuthread = new cuThread();
			startflg = true;
		}else{
			return null;
		}
		return cuthread;
	}
	// log data make func
	public static synchronized void setLog(String log){
		try {
			RandomAccessFile access = new RandomAccessFile("./exchange_log.txt", "rw");
			access.seek(access.length());
			access.writeBytes(log);
			access.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{
			// real time thread
			while(startcheckflg){
				/*
				 * if checkthreadflg is false, that means that Getdata thread killed
				 * So, GetData thread check and restart.
				 */
				if(!checkthreadflg){
					cuthread = Instance();
					if(cuthread != null){
						cuthread.start();
						Calendar c = Calendar.getInstance();
						setLog(c.get(Calendar.YEAR)+":"+c.get(Calendar.MONTH)+":"+c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.HOUR)+":"+c.get(Calendar.MINUTE)+": GetData Thread Starting\r\n");
					}
				}
				  Thread.sleep(1000 * 1);
			}
		}catch(Exception e){
			//startflg = false;
			System.out.println("CheckThread exception" + e.getStackTrace());
			startcheckflg = false;
			checkthread = null;
			Calendar c = Calendar.getInstance();
			setLog(c.get(Calendar.YEAR)+":"+c.get(Calendar.MONTH)+":"+c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.HOUR)+":"+c.get(Calendar.MINUTE)+": Monitor Thread Exception generate\r\n");
		}finally{
			startcheckflg = false;
			checkthread = null;
			Calendar c = Calendar.getInstance();
			setLog(c.get(Calendar.YEAR)+":"+c.get(Calendar.MONTH)+":"+c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.HOUR)+":"+c.get(Calendar.MINUTE)+": Monitor Thread Stop\r\n");
		}
	}
	//GetData thread
	public class cuThread extends Thread{

		/**
		 * @param args
		 */
		@Override
		public void run() {
			// TODO Auto-generated method stub
			try{
				while(startflg){
					checkthreadflg = true;
					/*
					 * if startcheckflg is false, check thread restart.
					 */
					if(!startcheckflg){
						checkthread = cuCheckThread._Instance();
						if(checkthread != null){
							checkthread.start();
							Calendar c = Calendar.getInstance();
							setLog(c.get(Calendar.YEAR)+":"+c.get(Calendar.MONTH)+":"+c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.HOUR)+":"+c.get(Calendar.MINUTE)+": Monitor Thread Restarting\r\n");
						}
					}
					//Get data from yahoo 
					  int cudate = 0;
					  Date currentTime1 = new Date();		
						SimpleDateFormat df1 = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
						String strdate1 = df1.format(currentTime1);
						SimpleDateFormat sourceFormat1 = new SimpleDateFormat("MM/dd/yyyy hh:mm aa"); 
						sourceFormat1.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
				        Date parsed1;
						try {
							parsed1 = sourceFormat1.parse(strdate1);
					        SimpleDateFormat destFormat = new SimpleDateFormat("dd"); 

					        cudate = Integer.parseInt(destFormat.format(parsed1));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							cudate = lastdate;
						} 
					  try{
						  for(int i = 0; i < codes.length; i++){
							  String urlFile = "http://download.finance.yahoo.com/d/quotes.csv?s=USD"+codes[i]+"=x&f=d1t1l1&e=.csv";
						       URL url = new URL(urlFile);
						      //create the new connection 
						      HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
						      urlConnection.setRequestMethod("GET");
						      urlConnection.connect();
		
						      //pointer to the downloaded file path
						      String localFileName = "./ExRateValues/"+codes[i]+"_quotes.csv";
						      if(cudate != lastdate){
						    	  // remove data before 5 days. So, data increasing have not
						    	  removeLastData(localFileName, cudate, i);
						      }
						      //this is the actual downloaded file
						      File MyFilePtrDest = new File(localFileName);
						      RandomAccessFile access = new RandomAccessFile(MyFilePtrDest, "rw");
						      long a = MyFilePtrDest.length();
						      access.seek(MyFilePtrDest.length());
						      //this will be used in reading the data from the Internet
						      InputStream inputStream = urlConnection.getInputStream();
		
						      //this will be used to write the downloaded data into the file we created
						      //FileOutputStream fileOutput = new FileOutputStream(MyFilePtrDest);
		
						      byte[] buffer = new byte[2048];
						      int bufferLength = 0; //used to store a temporary size of the buffer
						      InputStreamReader inputreader = new InputStreamReader(inputStream);
						      BufferedReader buffreader = new BufferedReader(inputreader);
						      Date currentTime = new Date();		
								SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
								df.setTimeZone(TimeZone.getDefault());
								String strdate = df.format(currentTime);
								SimpleDateFormat sourceFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm"); 
								sourceFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
						        Date parsed;
						        String day = "";
						        String time = "";
						        long cutime = 0;
						        long gtime = 0;
						        long diff = 0;
						        String direct = "";
						        Date diffdate;
								try {
									parsed = sourceFormat.parse(strdate);
									Calendar c = Calendar.getInstance();
									c.setTime(currentTime);
									cutime =  c.getTimeInMillis();
									Calendar c1 = Calendar.getInstance();
									c1.setTime(parsed);
									gtime = c1.getTimeInMillis();
									diff = cutime - gtime;
									cutime = cutime + diff;
									Calendar c2 = Calendar.getInstance();
									c2.setTimeInMillis(cutime);
									Date realdate = c2.getTime();
							        SimpleDateFormat destFormat = new SimpleDateFormat("MM/dd/yyyy"); 

							        day = destFormat.format(realdate);
							        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm"); 

							        time = timeFormat.format(realdate);
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} 
						      //write buffer contents to file
						      String line = null;
						      while ((line = buffreader.readLine()) != null) {
						         //add the data in the buffer to the file in the file output stream (the file on the sd card
						    	  //access.writeBytes(buffer.toString());
						    	  //String aa = new String(buffer);
						    	  String[] b = line.split(",");
						    	  b[0] = day;
						    	  b[1] = time;
						    	  String aa = b[0]+" "+b[1]+","+b[2]+"\r\n";
						    	  access.writeBytes(aa);
						    	  //fileOutput.write(buffer, 0, bufferLength);
						      }
						      //access.writeBytes(File.separator);
						      inputStream.close();
						      //close the access file when done
						      access.close();
						      urlConnection.disconnect();
						  }	
					  }catch(Exception e){
						  System.out.println("thread exception1" + e.getStackTrace());
						  Calendar c1 = Calendar.getInstance();
						  setLog(c1.get(Calendar.YEAR)+":"+c1.get(Calendar.MONTH)+":"+c1.get(Calendar.DAY_OF_MONTH)+" "+c1.get(Calendar.HOUR)+":"+c1.get(Calendar.MINUTE)+": GetDate Thread Internal Exception generate\r\n");
						  startflg = false;
						  checkthreadflg = false;
						  cuthread = null;
					  }
				      lastdate = cudate;	
				      Thread.sleep(3000);				
				}
			}catch(Exception e){
				//startflg = false;
				System.out.println("thread exception2" + e.getStackTrace());
				Calendar c = Calendar.getInstance();
				setLog(c.get(Calendar.YEAR)+":"+c.get(Calendar.MONTH)+":"+c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.HOUR)+":"+c.get(Calendar.MINUTE)+": GetData Thread External Exception generate\r\n");
				startflg = false;
				checkthreadflg = false;
				cuthread = null;
			}finally{
				startflg = false;
				checkthreadflg = false;
				cuthread = null;
				Calendar c = Calendar.getInstance();
				setLog(c.get(Calendar.YEAR)+":"+c.get(Calendar.MONTH)+":"+c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.HOUR)+":"+c.get(Calendar.MINUTE)+": GetData Thread Stop\r\n");
			}
		}
		// data before 5 days remove func
		public boolean removeLastData(String filePath, int cudate, int i){
			String[] crates = new String[1024];
		      try {
		    	  int redate = cudate - 5;
		    	  if(redate < 1){
		    		  redate = 30 + redate;
		    	  }
		         // open the file for reading
		         InputStream instream = new FileInputStream(filePath);
		         // if file the available for reading
		         if (instream != null) {
		            // prepare the file for reading
		            InputStreamReader inputreader = new InputStreamReader(instream);
		            BufferedReader buffreader = new BufferedReader(inputreader);
		            
		            
		            String localFileName = "./ExRateValues/"+codes[i]+"_quotes_tmp.csv";
				      //this is the actual downloaded file
				      File MyFilePtrDest = new File(localFileName);
				      RandomAccessFile access = new RandomAccessFile(MyFilePtrDest, "rw");
				      long a = MyFilePtrDest.length();
				      access.seek(MyFilePtrDest.length());
				      //this will be used in reading the data from the Internet
				     
				      byte[] buffer = new byte[1024];
				      int bufferLength = 0; //used to store a temporary size of the buffer

				      
		            
		            //read the line
		            
		            String line = null;
		            boolean flg = false;
		            long a1 = 0;
		            while((line = buffreader.readLine()) != null){
		            	String[] s = line.split("/");
		            	if(Integer.parseInt(s[1]) >= redate && !flg){
		            		flg = true;
		            	}
		            	if(flg){
		            		access.write((line+"\r\n").getBytes(), 0, (line+"\r\n").getBytes().length);
					    	access.seek(a1 + (line+"\r\n").getBytes().length);
					    	a1 = a1 + (line+"\r\n").getBytes().length;
		            	}
		            }
		            access.close();
		            instream.close();
		            // change temp.scv to scv///////////////////////////////
		            //Log.d("download", "fileLine = " + fileLine);
		            File f = new File(filePath);
		            f.delete();
		           
		            FileInputStream inputStream = new FileInputStream(MyFilePtrDest);         

		            FileOutputStream outputStream = new FileOutputStream(filePath);

		            FileChannel fcin =  inputStream.getChannel();

		            FileChannel fcout = outputStream.getChannel();
		            long size = fcin.size();

		            fcin.transferTo(0, size, fcout);
		            fcout.close();
		            fcin.close();
		            outputStream.close();
		            inputStream.close();
		            MyFilePtrDest.delete();


		         }
		      }catch(Exception e){
		    	  
		      }
		      return true;
		}

	}

}
